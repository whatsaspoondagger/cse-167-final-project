#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	This demo demonstrates lighting, camera movement, creating primitive objects
	still, there are some bugs,  ( movement of lights )
*/

int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"First Iprogram",512,512);
  Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::speed_factor = 0.1;
  Camera::type = Camera::FREE;

  /// Test directional light
  DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
  Iprogram::world.addChild(&p);

  /// Load a Bmp image
  GLuint tex_id1 = TextureManager::loadBMP("resources/textures/stone_wall.bmp",GL_NEAREST,GL_NEAREST);
  
  /// Create a cube
  Cube* cb = new Cube(1,1,1,0,0,0);
  cb->transform.selfRotate(0,Vector3(0,1,0),0,360,300,false);
  cb->setTexture(tex_id1);
  Iprogram::world.addChild(cb);

  /// Load PNG file
  GLuint tex_id2 = TextureManager::loadPNG("resources/textures/earth.png",GL_NEAREST,GL_NEAREST);
  
  /// Create a sphere
  Sphere* sp = new Sphere(1,10,10,3,0,0);
  sp->transform.setAngle(Vector3(1,0,0),-90);
  sp->transform.selfRotate(0,Vector3(0.2,1,0),0,360,300,false);
  sp->setTexture(tex_id2);
  Iprogram::world.addChild(sp);
  
  /// Create a cone
  Cone* ce = new Cone(1,1,4,-3,0,0);
  ce->setTexture(tex_id2);
  Iprogram::world.addChild(ce);

  /// Create a cylinder
  Cylinder* cr = new Cylinder(1,1,2,20,0,3,0);
  cr->transform.selfRotate(-90,Vector3(1,0,0),-90,90,300,false);
  cr->setTexture(tex_id2);
  Iprogram::world.addChild(cr);

  /// Load a Bmp image
  GLuint tex_id3 = TextureManager::loadBMP("resources/textures/water.bmp",GL_LINEAR,GL_LINEAR);
  /// Create a hemisphere
  HemiSphere* he = new HemiSphere(1,10,10,0,-3,0);
  //he->transform.selfRotate(-90,Vector3(1,0,0),-90,90,300,false);
  he->setTexture(tex_id3);
  Iprogram::world.addChild(he);

  /// Create a plane
  Plane* pl = new Plane(1,1,0,0,0);
  pl->setTexture(tex_id3);
  Iprogram::world.addChild(pl);

  /*
     Test Camera
  */
  vector<Vector3> positions,targets;

  positions.push_back(Vector3(10,0,20));
  positions.push_back( Vector3(20,4,20));
  positions.push_back( Vector3(20,4,-20));
  positions.push_back( Vector3(10,0,-20));
  
  positions.push_back( Vector3(-20,-1,-20));
  positions.push_back( Vector3(-20,-1,20));
  positions.push_back( Vector3(10,0,20));

  targets.push_back(Vector3(5,0,10));
  targets.push_back( Vector3(10,4,10));
  targets.push_back( Vector3(10,4,-10));
  targets.push_back( Vector3(5,0,-10));
  
  targets.push_back( Vector3(-10,-1,-10));
  targets.push_back( Vector3(-10,-1,10));
  targets.push_back( Vector3(5,0,10));

  Camera::createBezierCamera(positions,targets);
  Bezier::DRAWING_CURVE = true;

  Iprogram::start();

  return 0;
}