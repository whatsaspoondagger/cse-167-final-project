#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
This shows how to create Procedural Robot
*/

int main(int argc, char *argv[])
{
	Iprogram::init(argc,argv,"First Iprogram",512,512);
	//Geode::BOUNDING_BOX = true;
	//Geode::DRAWING_AXISES = true;
	Camera::type = Camera::FREE;
	Iprogram::useSkyBox = true;
	Iprogram::sky.load("cubemap_abraham");

	/// Test directional light
	DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
	Iprogram::world.addChild(&p);

	/// Load a Bmp image

	//Shader* sh = ShaderManager::addShader("resources/shaders/old_movie.vert","resources/shaders/old_movie.frag");

	GLuint tex_id1 = TextureManager::loadBMP("resources/textures/metal.bmp",GL_NEAREST,GL_NEAREST);
	Shader* sh = ShaderManager::addShader("resources/shaders/reflection.vert","resources/shaders/reflection.frag");

	for(int i=0; i<1;++i){
		Robot* robot = new Robot();
		robot->setTexture(tex_id1);
		robot->setShader(sh);
		robot->transform.setPosition(i*400,0,0);

		Iprogram::world.addChild(robot);
	}

	Iprogram::start();

	return 0;
}