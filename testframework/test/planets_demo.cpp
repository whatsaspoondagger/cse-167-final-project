#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "Iprogram.h"

using namespace std;
using namespace IFramework;

/*
	Just a demo of a list of planets to test different textures
*/

int main(int argc, char *argv[])
{
  Iprogram::init(argc,argv,"First Iprogram",512,512);
  //Geode::BOUNDING_BOX = true;
  //Geode::DRAWING_AXISES = true;
  Camera::type = Camera::FREE;

  //Iprogram::sky.load("cubemap_abraham");

  /// Test directional light
  DirectionalLight p(GL_LIGHT0,Vector3(10,10,10));
  Iprogram::world.addChild(&p);
  
  /// Load a Bmp image
  GLuint tex_id1 = TextureManager::loadBMP("resources/textures/earthmap.bmp",GL_LINEAR,GL_LINEAR);
  //Shader* sh1 = ShaderManager::addShader("resources/shaders/glow.vert","resources/shaders/glow.frag");
  
  GLuint tex_id2 = TextureManager::loadBMP("resources/textures/cloudsmap.bmp",GL_LINEAR,GL_LINEAR);
  Shader* sh2 = ShaderManager::addShader("resources/shaders/atmosphere.vert","resources/shaders/atmosphere.frag");
    
  Sphere* theEarth = new Sphere(100,100,100,0,0,0);
  theEarth->setTexture(tex_id1);
  //theEarth->setShader(sh1);
  Iprogram::world.addChild(theEarth);

  Sphere* atmosphere = new Sphere(104,100,100,0,0,0);
  atmosphere->drawing_doublefaces = true;
  atmosphere->setTexture(tex_id2);
  atmosphere->setShader(sh2);

  Iprogram::world.addChild(atmosphere);

  GLuint tex_id3 = TextureManager::loadBMP("resources/textures/moon.bmp",GL_LINEAR,GL_LINEAR);
  Sphere* moon = new Sphere(100,100,100,300,0,0);
  moon->transform.setAngle(Vector3(1,-0.1,0),-90);
  moon->setTexture(tex_id3);
  Iprogram::world.addChild(moon);

  GLuint tex_id4 = TextureManager::loadBMP("resources/textures/mars.bmp",GL_LINEAR,GL_LINEAR);
  Sphere* mars = new Sphere(100,100,100,600,0,0);
  mars->transform.setAngle(Vector3(1,-0.1,0),-90);
  mars->setTexture(tex_id4);
  Iprogram::world.addChild(mars);

  GLuint tex_id5 = TextureManager::loadBMP("resources/textures/sun.bmp",GL_LINEAR,GL_LINEAR);
  Sphere* sun = new Sphere(100,100,100,-300,0,0);
  sun->transform.setAngle(Vector3(1,-0.1,0),-90);
  sun->setTexture(tex_id5);
  Iprogram::world.addChild(sun);

  Iprogram::start();

  return 0;
}