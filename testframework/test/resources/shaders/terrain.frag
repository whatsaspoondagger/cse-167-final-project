varying vec4 position;
varying vec4 normal;
vec4 diffuse,ambient;
varying vec3 normalVector;

uniform int usingLightMap;
uniform sampler2D region1ColorMap;
uniform sampler2D region2ColorMap;
uniform sampler2D region3ColorMap;
//uniform sampler2D region4ColorMap;

struct TerrainRegion
{
    float min;
    float max;
};

TerrainRegion region1;
TerrainRegion region2;
TerrainRegion region3;
TerrainRegion region4;

//static const float3 light_dir = normalize(float3(-0.79f, 0.39f, 0.47f));

struct FogInfo {
	float maxDist;
	float minDist;
	vec3 color;
};
FogInfo Fog;


vec4 GenerateTerrainColor()
{
	/*
	/// Color the terrain using heights, doesn't look good.
	
	region1.min = 0.0;
	region1.max = 40.0;
	
	region2.min = 40.0;
	region2.max = 150.0;
	
	region3.min = 150.0;
	region3.max = 200.0;
	
	region4.min = 200.0;
	region4.max = 255.0;
	
    vec4 terrainColor = vec4(0.0, 0.0, 0.0, 1.0);
    float height = normal.w;
    float regionMin = 0.0;
    float regionMax = 0.0;
    float regionRange = 0.0;
    float regionWeight = 0.0;
    
    // Terrain region 1.
    regionMin = region1.min;
    regionMax = region1.max;
    regionRange = regionMax - regionMin;
    regionWeight = (regionRange - abs(height - regionMax)) / regionRange;
    regionWeight = max(0.0, regionWeight);
    terrainColor += regionWeight * texture2D(region1ColorMap, gl_TexCoord[0].st);

    // Terrain region 2.
    regionMin = region2.min;
    regionMax = region2.max;
    regionRange = regionMax - regionMin;
    regionWeight = (regionRange - abs(height - regionMax)) / regionRange;
    regionWeight = max(0.0, regionWeight);
    terrainColor += regionWeight * texture2D(region2ColorMap, gl_TexCoord[0].st);

    // Terrain region 3.
    regionMin = region3.min;
    regionMax = region3.max;
    regionRange = regionMax - regionMin;
    regionWeight = (regionRange - abs(height - regionMax)) / regionRange;
    regionWeight = max(0.0, regionWeight);
    terrainColor += regionWeight * texture2D(region3ColorMap, gl_TexCoord[0].st);

    // Terrain region 4.
    regionMin = region4.min;
    regionMax = region4.max;
    regionRange = regionMax - regionMin;
    regionWeight = (regionRange - abs(height - regionMax)) / regionRange;
    regionWeight = max(0.0, regionWeight);
    terrainColor += regionWeight * texture2D(region4ColorMap, gl_TexCoord[0].st);
	*/
	
	vec4 grass_color = texture2D(region2ColorMap, gl_TexCoord[0].st);
	vec4 rock_color = texture2D(region3ColorMap, gl_TexCoord[0].st);
	
	if(usingLightMap==0){
	//grass_color.r *= 1.2;
	//rock_color *= 5.0;
	}
	
	float blend_mask = clamp(normalVector.y * 8.0 - 6.0 + grass_color.g, 0.0, 1.0);
	
	vec4 terrainColor = mix(rock_color, grass_color, blend_mask);
	
    return terrainColor;
}


vec4 ads()
{
	vec3 n = normalize( normal.xyz );
	
	vec3 s = normalize( vec3(gl_LightSource[0].position) );
	vec3 v = normalize(vec3(-position));
	
	float NdotL = max(dot(n,s),0.0) + 0.1;
	
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	
	vec4 color = ambient *2.0;
	color += diffuse * NdotL ;
	
	if(usingLightMap==1){
		vec4 light_color = texture2D(region1ColorMap, gl_TexCoord[1].st);
		color  *=  (light_color*1.3 + 0.1);
	}else{
		color  *=  (1.3 + 0.1);
	}
	return color;
}

void main()
{   
	vec4 color = ads()* GenerateTerrainColor();
	
	Fog.minDist = 0.0;
	Fog.maxDist = 16000.0;
	Fog.color = vec3(0.5,0.5,0.5);
	
	float dist = length( position.xyz );
	float fogFactor = (Fog.maxDist - dist) /	(Fog.maxDist - Fog.minDist);
	fogFactor = clamp( fogFactor, 0.0, 1.0 );
	
	vec3 frag_color = mix( Fog.color, color.xyz, fogFactor );
	
    gl_FragColor = vec4(frag_color,1.0) ;
}