varying vec3 normal,lightDir,eye;
varying float dist;

void main()
{
	vec4 ecPos;
	vec3 aux;
	normal = normalize(gl_NormalMatrix * gl_Normal);

	eye = vec3(gl_ModelViewMatrix * gl_Vertex);
	ecPos = gl_ModelViewMatrix * gl_Vertex;
	aux = vec3(gl_LightSource[1].position-ecPos);
	
	lightDir = normalize(aux);
	dist = length(aux);
	
	gl_Position = ftransform();

}