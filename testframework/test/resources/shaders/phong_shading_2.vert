varying vec3 normal,lightDir0,lightDir1,eye;
varying float dist0,dist1;

void main()
{
	vec4 ecPos;
	vec3 aux;
	normal = normalize(gl_NormalMatrix * gl_Normal);

	ecPos = gl_ModelViewMatrix * gl_Vertex;
	eye = vec3(ecPos);
		
	aux = vec3(gl_LightSource[0].position-ecPos);
	lightDir0 = normalize(aux);
	dist0 = length(aux);

	aux = vec3(gl_LightSource[1].position-ecPos);
	lightDir1 = normalize(aux);
	dist1 = length(aux);
		
	gl_Position = ftransform();

}