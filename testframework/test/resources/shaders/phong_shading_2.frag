varying vec4 diffuse,ambientGlobal, ambient;
varying vec3 normal,lightDir0,lightDir1,eye;
varying float dist0,dist1;

void main()
{
	vec4 color = gl_LightModel.ambient ;
    vec3 E = normalize(-eye);	
    
	/*
		First light
	*/
	float att0,NdotL0;
    vec3 R0 = normalize(-reflect(lightDir0,normal));
    
	NdotL0 = max(dot(normal,lightDir0),0.0);
	
	att0 = 1.0 / (gl_LightSource[0].constantAttenuation + gl_LightSource[0].linearAttenuation * dist0 + gl_LightSource[0].quadraticAttenuation * dist0 * dist0);
	
	color += att0*( gl_LightSource[0].ambient*gl_FrontMaterial.ambient);
	color +=  att0*(gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * NdotL0);
	
	color +=  att0*( gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(max(dot(R0,E),0.0),gl_FrontMaterial.shininess) );
    
	/*
		Second light
	*/
	float att1,NdotL1,spotEffect;
    vec3 R1 = normalize(-reflect(lightDir1,normal));
    
	NdotL1 = max(dot(normal,lightDir1),0.0);

	spotEffect = dot(normalize(gl_LightSource[1].spotDirection),normalize(-lightDir1));
	
	if (spotEffect > gl_LightSource[1].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect = pow(spotEffect, gl_LightSource[1].spotExponent);
	   
		att1 = spotEffect / (gl_LightSource[1].constantAttenuation + gl_LightSource[1].linearAttenuation * dist1 + gl_LightSource[1].quadraticAttenuation * dist1 * dist1);
	
		color += att1*( gl_LightSource[1].ambient*gl_FrontMaterial.ambient);
		color +=  att1*(gl_FrontMaterial.diffuse * gl_LightSource[1].diffuse * NdotL1);
	
		color +=  att1*( gl_FrontMaterial.specular * gl_LightSource[1].specular * pow(max(dot(R1,E),0.0),gl_FrontMaterial.shininess) );
	}
        
        
	gl_FragColor = color;
	
}