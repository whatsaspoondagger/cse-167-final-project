#version 120
varying vec4 position;

uniform sampler2D tex;
void main()
{
	vec4 texColor = texture2D(tex,gl_TexCoord[0].st);
	if ( (texColor.r < 0.6) && (texColor.g < 0.6) && (texColor.b < 0.6)) {
		texColor.a = 0.3;
	}
	
	if ( (texColor.r < 0.4) && (texColor.g < 0.4) && (texColor.b < 0.4)) {
		texColor.a = 0.1;
	}
	
	if ( (texColor.r < 0.2) && (texColor.g < 0.2) && (texColor.b < 0.2)) {
		texColor.a = 0;
	}	

   vec4 sum = vec4(0);
   
   int j;
   int i;
   
   for( i= -4 ;i < 4; i++)
   {
        for (j = -3; j < 3; j++)
        {
            sum += texColor * 0.25;
        }
   }
       if (texColor.r < 0.3)
    {
       gl_FragColor = sum*sum*0.002 + texColor;
    }
    else
    {
        if (texColor.r < 0.5)
        {
            gl_FragColor = sum*sum*0.01 + texColor;
        }
        else
        {
            gl_FragColor = sum*sum*0.01 + texColor;
        }
    }
	
	//gl_FragColor = texColor ;
}