#version 120

varying vec3 Position;

void main()
{
	Position = (gl_Vertex).xyz;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}