	
uniform sampler2D colorTex;
uniform sampler2D normalMap;
varying vec4 position;

varying vec3 LightDir;
varying vec3 ViewDir;

struct FogInfo {
	float maxDist;
	float minDist;
	vec3 color;
};
FogInfo Fog;


vec3 phongModel( vec3 norm, vec3 diffR ) {

    vec3 r = reflect( -LightDir, norm );
    vec4 ambient = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
    float sDotN = max( dot(LightDir, norm), 0.0 );
    vec4 diffuse = gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * vec4(diffR,0) * sDotN;
	
    vec4 spec = vec4(0.0);
    if( sDotN > 0.0 )
        spec = gl_LightSource[0].specular * gl_FrontMaterial.specular *
               pow( max( dot(r,ViewDir), 0.0 ), gl_FrontMaterial.shininess );
			   
    return vec3(diffuse + spec*0.001);
}

void main()
{
	/// calculate fog
	Fog.minDist = 0.0;
	Fog.maxDist = 16000.0;
	Fog.color = vec3(0.5,0.5,0.5);
	
	float dist = length( position.xyz );
	float fogFactor = (Fog.maxDist - dist) /	(Fog.maxDist - Fog.minDist);
	fogFactor = clamp( fogFactor, 0.0, 1.0 );
		
    // Lookup the normal from the normal map
    vec4 normal = texture2D( normalMap, gl_TexCoord[0].st );
    vec4 texColor = texture2D( colorTex, gl_TexCoord[0].st );

	vec4 color = vec4( phongModel(normal.xyz, texColor.rgb), 1.0 );
	
	vec3 frag_color = mix( Fog.color, color.xyz, fogFactor );
	
	gl_FragColor = vec4(frag_color,1.0) ;
}