varying vec4 diffuse,ambientGlobal, ambient;
varying vec3 normal,lightDir0,lightDir1,lightDir2,lightDir3,lightDir4,eye;
varying float dist0,dist1;

void main()
{
	vec4 color = gl_LightModel.ambient ;
    vec3 E = normalize(-eye);	
    
	/*
		First light
	*/
	float att0,NdotL0;
    vec3 R0 = normalize(-reflect(lightDir0,normal));
    
	NdotL0 = max(dot(normal,lightDir0),0.0);
	
	att0 = 1.0 / (gl_LightSource[0].constantAttenuation + gl_LightSource[0].linearAttenuation * dist0 + gl_LightSource[0].quadraticAttenuation * dist0 * dist0);
	
	color += att0*( gl_LightSource[0].ambient*gl_FrontMaterial.ambient);
	color +=  att0*(gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * NdotL0);
	
	color +=  att0*( gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(max(dot(R0,E),0.0),gl_FrontMaterial.shininess) );
    
	/*
		Second light
	*/
	float att1,NdotL1,spotEffect;
    vec3 R1 = normalize(-reflect(lightDir1,normal));
    
	NdotL1 = max(dot(normal,lightDir1),0.0);

	spotEffect = dot(normalize(gl_LightSource[1].spotDirection),normalize(-lightDir1));
	
	if (spotEffect > gl_LightSource[1].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect = pow(spotEffect, gl_LightSource[1].spotExponent);
	   
		att1 = spotEffect / (gl_LightSource[1].constantAttenuation + gl_LightSource[1].linearAttenuation * dist1 + gl_LightSource[1].quadraticAttenuation * dist1 * dist1);
	
		color += att1*( gl_LightSource[1].ambient*gl_FrontMaterial.ambient);
		color +=  att1*(gl_FrontMaterial.diffuse * gl_LightSource[1].diffuse * NdotL1);
	
		color +=  att1*( gl_FrontMaterial.specular * gl_LightSource[1].specular * pow(max(dot(R1,E),0.0),gl_FrontMaterial.shininess) );
	}
    
	/*
		Third light
	*/
	float NdotL2,spotEffect2;
    vec3 R2 = normalize(-reflect(lightDir2,normal));
    
	NdotL2 = max(dot(normal,lightDir2),0.0);

	spotEffect2 = dot(normalize(gl_LightSource[2].spotDirection),normalize(-lightDir2));
	
	if (spotEffect2 > gl_LightSource[2].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect2 = pow(spotEffect2, gl_LightSource[2].spotExponent);
	   
		color += spotEffect2*( gl_LightSource[2].ambient*gl_FrontMaterial.ambient);
		color +=  spotEffect2*(gl_FrontMaterial.diffuse * gl_LightSource[2].diffuse * NdotL2);
	
		color +=  spotEffect2*( gl_FrontMaterial.specular * gl_LightSource[2].specular * pow(max(dot(R2,E),0.0),gl_FrontMaterial.shininess) );
	}
 
	/*
		Fourth light
	*/
	float NdotL3,spotEffect3;
    vec3 R3 = normalize(-reflect(lightDir3,normal));
    
	NdotL3 = max(dot(normal,lightDir3),0.0);

	spotEffect3 = dot(normalize(gl_LightSource[3].spotDirection),normalize(-lightDir3));
	
	if (spotEffect3 > gl_LightSource[3].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect3 = pow(spotEffect3, gl_LightSource[3].spotExponent);
	   
		color += spotEffect3*( gl_LightSource[3].ambient*gl_FrontMaterial.ambient);
		color +=  spotEffect3*(gl_FrontMaterial.diffuse * gl_LightSource[3].diffuse * NdotL3);
	
		color +=  spotEffect3*( gl_FrontMaterial.specular * gl_LightSource[3].specular * pow(max(dot(R3,E),0.0),gl_FrontMaterial.shininess) );
	}
	
	/*
		Fifth light
	*/
	float NdotL4,spotEffect4;
    vec3 R4 = normalize(-reflect(lightDir4,normal));
    
	NdotL4 = max(dot(normal,lightDir4),0.0);

	spotEffect4 = dot(normalize(gl_LightSource[4].spotDirection),normalize(-lightDir4));
	
	if (spotEffect4 > gl_LightSource[4].spotCosCutoff) {
		/* compute the illumination in here */
	    spotEffect4 = pow(spotEffect4, gl_LightSource[4].spotExponent);
	   
		color += spotEffect4*( gl_LightSource[4].ambient*gl_FrontMaterial.ambient);
		color +=  spotEffect4*(gl_FrontMaterial.diffuse * gl_LightSource[4].diffuse * NdotL4);
	
		color +=  spotEffect4*( gl_FrontMaterial.specular * gl_LightSource[4].specular * pow(max(dot(R4,E),0.0),gl_FrontMaterial.shininess) );
	}
	
		        
	gl_FragColor = color;
	
}