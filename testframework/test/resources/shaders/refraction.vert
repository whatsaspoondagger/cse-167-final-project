#version 120

/// This method doesn't calculate camera position
varying vec3  ReflectDir;
varying vec3  RefractDir;

varying vec3 normal,lightDir,eye;
varying float LightIntensity;

void main()
{
    
    normal    = normalize(gl_NormalMatrix * gl_Normal);
    eye       = vec3(gl_ModelViewMatrix * gl_Vertex);
    vec3 eyeDir    = normalize(eye.xyz);
    ReflectDir     = reflect(eyeDir, normal);
	RefractDir = refract(eyeDir, normal, 0.94f );
	
	lightDir = vec3(gl_LightSource[0].position.xyz -eye);
	
    //LightIntensity = max(dot(normalize(gl_LightSource[0].position.xyz - eyeDir), normal),0.0);
	
	gl_Position    = ftransform();
}
