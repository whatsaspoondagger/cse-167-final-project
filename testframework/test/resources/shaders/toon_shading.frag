#version 120
varying vec4 position;
varying vec4 normal;
vec4 diffuse,ambient;

uniform sampler2D tex;

const int levels = 4;
const float scaleFactor = 1.0 / float(levels);

vec4 ads()
{
	vec3 n = normalize( normal.xyz );
	
	vec3 s = normalize( vec3(gl_LightSource[0].position) );
	vec3 v = normalize(vec3(-position));
	
	float NdotL = max(dot(n,s),0.0);
	
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	
	vec4 color = ambient;
	color += diffuse * floor( NdotL * float(levels) ) * scaleFactor;
	
	return color;
}

void main()
{
	vec4 color = ads();
	
	/* Calculating outline here
    if (dot(position, normal)  < mix(0.4, 0.1, 
     max(0.0, dot(normal, gl_LightSource[0].position))))
    {
        gl_FragColor = color * vec4(0,0,0,1) * texture2D(tex, gl_TexCoord[0].st); 
    }else{
		
	}
	*/
	
	gl_FragColor = color* texture2D(tex, gl_TexCoord[0].st);
}