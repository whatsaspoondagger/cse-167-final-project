varying vec4 normal;
varying vec4 position;
varying vec3 normalVector;
uniform int usingLightMap;

void main()
{
    normal.xyz = normalize(gl_NormalMatrix * gl_Normal);
	normalVector = gl_Normal;
    normal.w = gl_Vertex.y;
	
	position = gl_ModelViewMatrix * gl_Vertex;
	
    gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_TexCoord[0].s = gl_Vertex.x/(32.0*8.0);
	gl_TexCoord[0].t = gl_Vertex.z/(32.0*8.0);
	
	if(usingLightMap==1){
	gl_TexCoord[1] = gl_MultiTexCoord0;
	gl_TexCoord[1].s = gl_Vertex.x/(256.0*16.0);
	gl_TexCoord[1].t = gl_Vertex.z/(256.0*16.0);
	}
	
    gl_Position = ftransform();
}