#ifndef _FRUSTUM_
#define _FRUSTUM_

#include <Core\Math\Vector3.h>
#include <Cameras\FrustumPlane.h>
#include <Objects\BoundingBox.h>
#include <math.h>
#include <GL/glut.h>
#include <Core\Math\Vector3.h>

namespace IFramework {

class Frustum 
{
private:
	enum {
		TOP = 0,
		BOTTOM,
		LEFT,
		RIGHT,
		NEARP,
		FARP
	};

public:
	static enum {OUTSIDE, INTERSECT, INSIDE};

	static FrustumPlane pl[6];
	static Vector3 ntl,ntr,nbl,nbr,ftl,ftr,fbl,fbr;
	static float fov, nearD, farD;
	static float nw,nh,fw,fh;

	static int drawn;

	static void setSettings(float angle, float ratio, float near, float far);
	static void setCamera(Vector3 &p, Vector3 &l, Vector3 &u);
	static int pointInFrustum(Vector3 &p);
	static int sphereInFrustum(Vector3 &p, float raio);
	static int boxInFrustum(BoundingBox &b);
	static void drawLines();
};

}
#endif