#ifndef _SHADERMANAGER_H_
#define _SHADERMANAGER_H_

#include <vector>
#include <Shaders\Shader.h>
#include <Scenes\Geode.h>

namespace IFramework{

class ShaderManager
{
private:
	static vector<Shader*> shaders;
public:
	ShaderManager();
	static Shader* addShader(const char* vertexShader,const char* fragmentShader);
	static void removeShader(Geode* model);
};

}

#endif