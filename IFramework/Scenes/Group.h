#ifndef _GROUP_H_
#define _GROUP_H_

#include <list>
#include "Node.h"
#include <Shaders\Shader.h>
#include <GL/glut.h>
#include "Transform.h"
#include <Cameras\Camera.h>

namespace IFramework {

class Group : public Node
{
private:
	Shader* shader;
	bool hasTexture;
	bool hasShader;
	GLuint textureId;

  public:
	Transform transform;
	bool drawing_outline;

	Group();
    void addChild(Node* child) { children.push_back(child);}
    void removeChild(Node* child) { children.remove(child);}
    void removeChildren() { children.clear(); }
    int numChildren() { return (int)children.size(); }
    void draw(Matrix4 & m, Vector3 pos);
	void setShader(Shader* sh);
	void setTexture(GLuint&);


  protected:
    std::list<Node*> children;
};

}
#endif
