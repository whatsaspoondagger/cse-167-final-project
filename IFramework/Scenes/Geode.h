#ifndef _GEODE_H_
#define _GEODE_H_

#include <Shaders\Shader.h>
#include <Scenes\Node.h>
#include <Objects\BoundingBox.h>
#include <Materials\Material.h>
#include <Cameras\Frustum.h>
#include <Scenes\Transform.h>
#include <Cameras\Camera.h>

namespace IFramework {

class Geode : public Node
{
  protected:

	GLuint textureId;
	bool hasTexture;
	bool hasShader;

	void drawBoundingBox();
	void drawAxises();

public:
	Transform transform;
	static bool DRAWING_AXISES;
	static bool BOUNDING_BOX;

	bool drawing_outline;
	bool drawing_backface;
	bool drawing_doublefaces;
	bool disable_depth;
	bool disable_light;

	BoundingBox box;
	Material material;
	Shader*	shader;

	Geode();
	Geode(double x ,double y, double z);

	void init();
    void draw(Matrix4 & m, Vector3 pos);

	void setTexture(GLuint&);
	void setShader(Shader* shader);

    virtual void calculateBounding() = 0;
    virtual void render() = 0;
};

}

#endif
