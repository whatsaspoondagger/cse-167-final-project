#ifndef _NODE_H_
#define _NODE_H_


#include <Core\Math\Matrix4.h>
#include <Scenes\Transform.h>

namespace IFramework {

class Node
{
  public:
    virtual void draw(Matrix4 & m, Vector3 pos) = 0;
};

}
#endif
