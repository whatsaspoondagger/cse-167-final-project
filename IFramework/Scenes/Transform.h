#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include <Core/Math/Matrix4.h>
#include <Core/Math/Vector3.h>

namespace IFramework {

class Transform
{
private:
  bool repeat;
  void updateRotation(double& degree, double& from_degree, double& to_degree, double& speed);
  
public:
	/// Self Rotation
	Vector3 self_angle_axis;
	double self_angle, self_from_angle, self_to_angle, self_angle_speed;

	/// Rotation
	Vector3 angle_axis;
	double angle, from_angle, to_angle, angle_speed;

	/// Translation
	Vector3 position, direction;
	double distance,cur_position, move_speed;

	Transform();
	Transform(double x,double y, double z);
	void init(double x,double y, double z);
	void update();
	void animate();
	void setPosition(double x, double y, double z);
	void setAngle(Vector3 axis, double angle);
	void selfRotate(double start, Vector3 axis,double from, double to, double time, bool repeat);
	void rotate(Vector3 axis,double from, double to, double time);
	void move(Vector3 from, Vector3 to, double time);
	Matrix4 Transform::getMatrix();
};

}
#endif