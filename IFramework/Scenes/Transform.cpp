#include "Transform.h"
#include "Group.h"
#include <GL\glut.h>
#include <iostream>

namespace IFramework {

	Transform::Transform()
	{
		init(0,0,0);
	}

	Transform::Transform(double x, double y, double z)
	{
		init(x,y,z);
	}

	void Transform::init(double x,double y, double z){

		repeat = false;
		position.x = x;
		position.y = y;
		position.z = z;

		self_angle = self_from_angle = self_to_angle = self_angle_speed = 0;
		self_angle_axis = Vector3(0,0,0);

		angle = from_angle = to_angle = angle_speed = 0;
		angle_axis = Vector3(0,0,0);

		cur_position = distance = move_speed = 0;
		direction = Vector3(0,0,0);
	}

	void Transform::animate(){

		Vector3 translation = position + direction*cur_position;
		glRotated(angle,angle_axis.x, angle_axis.y, angle_axis.z);
		glTranslated(translation.x, translation.y, translation.z);
		
		glRotated(self_angle,self_angle_axis.x, self_angle_axis.y, self_angle_axis.z);
	}

	void Transform::setPosition(double x, double y, double z){
		position.x = x;
		position.y = y;
		position.z = z;
	}

	void Transform::setAngle(Vector3 axis, double angle){
		self_angle_axis = axis;
		self_angle = angle;
	}

	void Transform::update(){
		/*
		update new position;
		*/
		if(cur_position>distance || cur_position<0){
			move_speed*=-1;
		}
		cur_position+=move_speed;

		updateRotation(self_angle,self_from_angle,self_to_angle,self_angle_speed);
		updateRotation(angle,from_angle,to_angle,angle_speed);
	}

	void Transform::updateRotation(double& degree, double& from_degree, double& to_degree, double& speed)
	{
		if(degree > to_degree){
			if(repeat){
				degree = from_degree;
			}else{
				speed*=-1;
			}
		}

		if(degree < from_degree){
			speed*=-1;
		}

		degree +=speed;
	}

	void Transform::selfRotate(double start, Vector3 axis,double from, double to, double time, bool repeat){
		self_angle_axis = axis;
		self_from_angle = from;
		self_angle = start;
		self_to_angle = to;
		this->repeat = repeat;
		self_angle_speed = (to - from)/time;
	}

	void Transform::rotate(Vector3 axis,double from, double to, double time){
		angle_axis = axis;
		from_angle = from;
		angle = from;
		to_angle = to;

		angle_speed = (to - from)/time;
	}

	void Transform::move(Vector3 from, Vector3 to, double time){
		direction = to - from;
		distance = (to-from).length();

		move_speed = distance/time;

	}

	Matrix4 Transform::getMatrix(){
		
		Matrix4 m;
		m.identity();

		Matrix4 rotate;
		rotate.rotate(angle,angle_axis.x,angle_axis.y,angle_axis.z);
		
		Matrix4 translate;
		Vector3 translation = position + direction*cur_position;
		translate.translate(position.x, position.y, position.z);
		
		Matrix4 self_rotate;
		self_rotate.rotate(self_angle,self_angle_axis.x, self_angle_axis.y, self_angle_axis.z);

		m = translate;

		return m;
	}

}