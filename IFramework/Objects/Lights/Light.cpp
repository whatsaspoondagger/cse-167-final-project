#include "Light.h"

namespace IFramework {

Light::Light(GLenum index, Vector4 pos): LightNode(pos.x,pos.y,pos.z)
{
   glEnable(index);
   this->index = index;

   light_pos[0] = pos.x;
   light_pos[1] = pos.y;
   light_pos[2] = pos.z;
   light_pos[3] = pos.w;

   ambient[0] = 0.4;
   ambient[1] = 0.4;
   ambient[2] = 0.4;
   ambient[3] = 1;

   specular[0] = 1;
   specular[1] = 1;
   specular[2] = 1;
   specular[3] = 1;

   diffuse[0] = 0.8;
   diffuse[1] = 0.8;
   diffuse[2] = 0.8;
   diffuse[3] = 1;

   constant_attenuation = 1;
   linear_attenuation = 0;
   quadratic_attenuation = 0;
}

void Light::setAmbient(float r, float g, float b){
   ambient[0] = r;
   ambient[1] = g;
   ambient[2] = b;
   ambient[3] = 1;
}

void Light::setDiffuse(float r, float g, float b){
   diffuse[0] = r;
   diffuse[1] = g;
   diffuse[2] = b;
   diffuse[3] = 1;
}

void Light::setSpecular(float r, float g, float b){
   specular[0] = r;
   specular[1] = g;
   specular[2] = b;
   specular[3] = 1;
}

void Light::setConstantAttenuation(float c){
	constant_attenuation = c;
}

void Light::setLinearAttenuation(float c){
	linear_attenuation = c;
}
 
void Light::setQuadraticAttenuation(float c){
	quadratic_attenuation = c;
}

void Light::render()
{
	glLightfv(index, GL_POSITION, light_pos);

	glLightfv(index, GL_AMBIENT, ambient);
	glLightfv(index, GL_DIFFUSE, diffuse);
	glLightfv(index, GL_SPECULAR, specular);

	glLightf(index, GL_CONSTANT_ATTENUATION, constant_attenuation);
	glLightf(index, GL_LINEAR_ATTENUATION, linear_attenuation);
	glLightf(index, GL_QUADRATIC_ATTENUATION, quadratic_attenuation);

	postRender();

	glColor3f(1,1,0);
    //glutSolidSphere(0.5, 20, 20);
}

}