#ifndef _DIRECTIONALLIGHT_H_
#define _DIRECTIONALLIGHT_H_

#include <Objects\Lights\Light.h>

namespace IFramework {

class DirectionalLight : public Light
{
  public:
    DirectionalLight(GLenum index, Vector3 pos);
    void postRender();
};

}

#endif