#include "SpotLight.h"

namespace IFramework {

SpotLight::SpotLight(GLenum index, Vector3 pos, Vector3 direction, float spot_cutoff):Light(index, Vector4(pos.x,pos.y,pos.z,1))
{
   spot_direction[0] = direction.x;
   spot_direction[1] = direction.y;
   spot_direction[2] = direction.z;
   spot_direction[3] = 0;

   this->spot_exponent = 64;
   this->spot_cutoff = spot_cutoff;
}

void SpotLight::setDirection(float x, float y, float z){
   spot_direction[0] = x;
   spot_direction[1] = y;
   spot_direction[2] = z;
   spot_direction[3] = 0;
}

void SpotLight::setExponent(float c){
	spot_exponent = c;
}

void SpotLight::setCutoff(float c){
	spot_cutoff = c;
}

void SpotLight::postRender()
{
	glLightf(index, GL_SPOT_CUTOFF, spot_cutoff);
	glLightf(index, GL_SPOT_EXPONENT, spot_exponent);
	glLightfv(index, GL_SPOT_DIRECTION, spot_direction);
}

}