#ifndef _LIGHT_H_
#define _LIGHT_H_

#include <Scenes\LightNode.h>

namespace IFramework {

class Light : public LightNode
{
  protected:
	unsigned int index;
	GLfloat light_pos[4];
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	float constant_attenuation;
	float linear_attenuation;
    float quadratic_attenuation;

  public:
    Light(GLenum index, Vector4 pos);
	void setAmbient(float r,float g,float b);
	void setSpecular(float r,float g,float b);
	void setDiffuse(float r,float g, float b);
	void setConstantAttenuation(float c);
	void setLinearAttenuation(float c);
    void setQuadraticAttenuation(float c);
	void render();
	void calculateBounding();

	virtual void postRender() = 0;
};

}

#endif