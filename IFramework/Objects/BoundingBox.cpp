#include "BoundingBox.h"
#include <Core\Math\Vector3.h>


namespace IFramework {

BoundingBox::BoundingBox( Vector3 &corner,  double x, double y, double z) {
	setBox(corner,x,y,z);
}

BoundingBox::BoundingBox(void) {
	corner.x = 0; corner.y = 0; corner.z = 0;

	x = 1.0f;
	y = 1.0f;
	z = 1.0f;
	
}

BoundingBox::~BoundingBox() {}

void BoundingBox::setBox( Vector3 &corner,  double x, double y, double z) {

	this->corner = corner;

	if (x < 0.0) {
		x = -x;
		this->corner.x -= x;
	}
	if (y < 0.0) {
		y = -y;
		this->corner.y -= y;
	}
	if (z < 0.0) {
		z = -z;
		this->corner.z -= z;
	}
	this->x = x;
	this->y = y;
	this->z = z;

	width = corner.x + x;
	height = corner.y + y;
	thick = corner.z + z;
}



Vector3 BoundingBox::getVertexP(Vector3 &normal) {

	Vector3 res = corner;

	if (normal.x > 0)
		res.x += x;

	if (normal.y > 0)
		res.y += y;

	if (normal.z > 0)
		res.z += z;

	return(res);
}



Vector3 BoundingBox::getVertexN(Vector3 &normal) {

	Vector3 res = corner;

	if (normal.x < 0)
		res.x += x;

	if (normal.y < 0)
		res.y += y;

	if (normal.z < 0)
		res.z += z;

	return(res);
}

void BoundingBox::print(){
	cout<<"Min: "<<corner.x<<" "<<corner.y<<" "<<corner.z<<endl;
	cout<<"Max: "<<x<<" "<<y<<" "<<z<<endl;
}

}
	
