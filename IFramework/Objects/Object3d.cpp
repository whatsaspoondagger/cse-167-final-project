#include "Object3d.h"
#include <Utils\objLoader.h>
#include <iostream>

using namespace std;

namespace IFramework {

	Object3d::Object3d(char* name, int type, bool hasNormals){

		switch(type){
		case OBJ:
			ObjLoader::readObj(name, numbVertices, &vertices, &normals, &texcoords, numbIndices, &indices);
			break;
		case COLLADA:
			//// Add collada reader here 
			break;
		}

		if(!hasNormals){
			calculateNormals();
		}

		calculateBounding();
	}

	void Object3d::render(){
		//  nVerts, &vertices, &normals, &texcoords, nIndices, &indices
		glBegin (GL_TRIANGLES) ;

		for(int i = 0;i <numbIndices;i+=3){
			int i0 = indices[i+0] * 3;
			int i1 = indices[i+1] * 3;
			int i2 = indices[i+2] * 3;

			glColor3f(1,1,1);
#if 0
			std::cout << normals[i0 + 0] << ", " << normals[i0 + 1] << ", " << normals[i0 + 2] << std::endl;
			std::cout << normals[i1 + 0] << ", " << normals[i1 + 1] << ", " << normals[i1 + 2] << std::endl;
			std::cout << normals[i2 + 0] << ", " << normals[i2 + 1] << ", " << normals[i2 + 2] << std::endl;
#endif
			
			if (texcoords != NULL)
				glTexCoord2d(texcoords[indices[i]*3],texcoords[indices[i]*3+1]);
			if (normals != NULL)
				glNormal3fv(normals + i0);
			glVertex3fv(vertices + i0);

			if (texcoords != NULL)
				glTexCoord2d(texcoords[indices[i+1]*3],texcoords[indices[i+1]*3+1]);
			if (normals != NULL)
				glNormal3fv(normals + i1);
			glVertex3fv(vertices + i1);

			if (texcoords != NULL)
				glTexCoord2d(texcoords[indices[i+2]*3],texcoords[indices[i+2]*3+1]);
			if (normals != NULL)
				glNormal3fv(normals + i2);
			glVertex3fv(vertices + i2);

		}

		glEnd () ;
	}

	void Object3d::calculateBounding(){

		Vector3 min_vec, max_vec;
		min_vec = Vector3(INT_MAX,INT_MAX,INT_MAX);
		max_vec = Vector3(INT_MIN,INT_MIN,INT_MIN);

		min_vec = Vector3(INT_MAX,INT_MAX,INT_MAX);
		max_vec = Vector3(INT_MIN,INT_MIN,INT_MIN);

		for(int i = 0; i < numbVertices; ++i){
			if(vertices[i*3]<min_vec.x) min_vec.x = vertices[i*3];
			if(vertices[i*3+1]<min_vec.y) min_vec.y = vertices[i*3+1];
			if(vertices[i*3+2]<min_vec.z) min_vec.z = vertices[i*3+2];

			if(vertices[i*3]>max_vec.x) max_vec.x = vertices[i*3];
			if(vertices[i*3+1]>max_vec.y) max_vec.y = vertices[i*3+1];
			if(vertices[i*3+2]>max_vec.z) max_vec.z = vertices[i*3+2];
		}

		/* SHOULD I ADD THIS ?
		position.x = (min_vec.x + max_vec.x)/2 ;
		position.y = (min_vec.y + max_vec.y)/2 ;
		position.z = (min_vec.z + max_vec.z)/2 ;
		*/

		box.corner = min_vec;
		box.x = max_vec.x;
		box.y = max_vec.y;
		box.z = max_vec.z;
	}

	void Object3d::calculateNormals(){

		hasNormals = true;
		normals = new float[numbIndices];
		for(int i = 0; i < numbIndices; i+=3){
			/// Vertice 1
			Vector3 v1 = Vector3(vertices[indices[i]*3],vertices[indices[i]*3+1],vertices[indices[i]*3+2]);
			Vector3 v2 = Vector3(vertices[indices[i+1]*3],vertices[indices[i+1]*3+1],vertices[indices[i+1]*3+2]);
			Vector3 v3 = Vector3(vertices[indices[i+2]*3],vertices[indices[i+2]*3+1],vertices[indices[i+2]*3+2]);

			Vector3 a = v2 - v1;
			Vector3 b = v3 - v1;
			Vector3 n;

			n.cross(a,b);
			n.normalize();

			normals[i] = n.x;
			normals[i+1] = n.y;
			normals[i+2] = n.z;
		}
	}

}