#include "Cube.h"

namespace IFramework {

Cube::Cube()
{
	this->width = 1;
	this->height = 1;

	calculateBounding();
	passingTangents = false;
}

Cube::Cube(float width,float height,float thick,float x,float y,float z):Geode(x,y,z)
{
	this->width = width;
	this->height = height;
	this->thick = thick;

	calculateBounding();
	passingTangents = false;
}

void Cube::setNormalMapping(char* colorFile, char* normalMap){

	passingTangents = true;
	colorTexId = TextureManager::loadBMP(colorFile,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
	normalMapId = TextureManager::loadBMP(normalMap,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
}

void Cube::render()
{
  if(passingTangents){
	glActiveTexture( GL_TEXTURE0 + 0 );
	glBindTexture( GL_TEXTURE_2D, colorTexId );

	glActiveTexture( GL_TEXTURE0 + 1);
	glBindTexture( GL_TEXTURE_2D, normalMapId );

	glUniform1i(glGetUniformLocation( shader->pid, "colorTex" ), 0 );
	glUniform1i(glGetUniformLocation( shader->pid, "normalMap" ), 1 );

	tangentLocation = glGetAttribLocationARB(shader->pid, "tangent");
  }

  glBegin(GL_QUADS);
        // Front Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,-1,0);
		}
		glNormal3f( 0.0f, 0.0f, 1.0f);                 
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-width/2, -height/2,  thick/2);
        glTexCoord2f(1.0f, 0.0f); glVertex3f( width/2, -height/2,  thick/2);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( width/2,  height/2,  thick/2);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-width/2,  height/2,  thick/2);

        // Back Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,1,0);
		}
        glNormal3f( 0.0f, 0.0f,-1.0f);                 
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-width/2, -height/2, -thick/2);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-width/2,  height/2, -thick/2);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( width/2,  height/2, -thick/2);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( width/2, -height/2, -thick/2);

        // Top Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,0,1);
		}
        glNormal3f( 0.0f, 1.0f, 0.0f);                 
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-width/2,  height/2, -thick/2);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-width/2,  height/2,  thick/2);
        glTexCoord2f(1.0f, 0.0f); glVertex3f( width/2,  height/2,  thick/2);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( width/2,  height/2, -thick/2);

        // Bottom Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation,  0,0,-1);
		}
        glNormal3f( 0.0f,-1.0f, 0.0f);                 
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-width/2, -height/2, -thick/2);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( width/2, -height/2, -thick/2);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( width/2, -height/2,  thick/2);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-width/2, -height/2,  thick/2);

        // Right Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,-1,0);
		}
        glNormal3f( 1.0f, 0.0f, 0.0f);                 
        glTexCoord2f(1.0f, 0.0f); glVertex3f( width/2, -height/2, -thick/2);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( width/2,  height/2, -thick/2);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( width/2,  height/2,  thick/2);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( width/2, -height/2,  thick/2);

        // Left Face
		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,-1,0);
		}
        glNormal3f(-1.0f, 0.0f, 0.0f);                 
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-width/2, -height/2, -thick/2);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-width/2, -height/2,  thick/2);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-width/2,  height/2,  thick/2);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-width/2,  height/2, -thick/2);
    glEnd();
}

void Cube::calculateBounding()
{
	box.corner = Vector3(-width/2,-height/2,-thick/2);
	box.x =  width/2;
	box.y = height/2;
	box.z = thick/2;
}

}
