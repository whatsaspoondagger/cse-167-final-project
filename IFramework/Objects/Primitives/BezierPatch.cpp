#include "BezierPatch.h"

namespace IFramework{

	BezierPatch::BezierPatch(const int size,const int grid_size,const int height){
		
		this->size = size;
		this->grid_size = grid_size;
		this->height = height;

		theNurb = gluNewNurbsRenderer();
		 gluNurbsProperty(theNurb, GLU_SAMPLING_METHOD, GLU_DOMAIN_DISTANCE);
		 gluNurbsProperty(theNurb, GLU_U_STEP, 5);
		gluNurbsProperty(theNurb, GLU_V_STEP, 5);
		glEnable(GL_AUTO_NORMAL);

		gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_FILL);

		int u, v;
		float f;

		//\ctlpoints = Utils::alloc_data(width, length, 3);
		
		srand((unsigned)time(NULL));
		for(u = 0; u < size; u++){
			for(v = 0; v < size; v++){
				ctlpoints[u][v][0] = grid_size *((GLfloat)u);
				ctlpoints[u][v][1] = grid_size *((GLfloat)v);

				if(u != 0 || u != 7){
					f = (float)(rand() % height - height/2);
					ctlpoints[u][v][2] = f;
				}else{
					ctlpoints[u][v][2] = -height/2;
				}
			}
		}

		int tex_size = 1;
		for(u = 0; u < size; u++){
			for(v = 0; v < size; v++){
				ctlpoints2[u][v][0] =tex_size * ((GLfloat)u);
				ctlpoints2[u][v][1] = tex_size * ((GLfloat)v);

			}
		}

		knots = new float[size+4];

		for(int k=0;k<size+4; ++k){
			knots[k] = k;
		}
		
		calculateBounding();
	}

	void BezierPatch::calculateBounding(){
		box.corner = transform.position;
		box.corner.z = -height/2;

		box.x = size;
		box.y = size;
		box.z = height/2;
	}

	void BezierPatch::render(){
		//GLfloat u_texknots[] = {0, 0, 1, 2, 2};
		//GLfloat v_texknots[] = {0, 0, 1, 1};
		GLfloat texture[] = {1, 0, 0.5, 0, 0, 0, 1, 1, 0.5, 1, 0, 1};

		gluBeginSurface(theNurb);

		gluNurbsSurface(theNurb,  size+4, knots, size+4, knots, size * 3,3, &ctlpoints2[0][0][0], 4, 4, GL_MAP2_TEXTURE_COORD_2);

		gluNurbsSurface(theNurb,
			size+4, knots,
			size+4, knots,
			size * 3,
			3,
			&ctlpoints[0][0][0],
			4, 4,
			GL_MAP2_VERTEX_3);
		gluEndSurface(theNurb);
		
	}

}