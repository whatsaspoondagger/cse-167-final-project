#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <Scenes\Geode.h>
#include <Utils\Utils.h>
#include <Textures\TextureManager.h>

namespace IFramework {

class Sphere : public Geode
{
  private:
	double (*vertices)[3];
	double (*normals)[3];
	double (*tangents)[3];
	double (*texcoords)[2];
	GLint tangentLocation;
	GLuint colorTexId;
	GLuint normalMapId;
	bool passingTangents;

  public:
	/// Variables
	float radius;
	int slices;
	int stacks;

	/// Methods
    Sphere();
    Sphere(float radius,int slices, int stacks,float x,float y,float z);
	
	void setNormalMapping( char* colorFile, char* normalMap);
	void createSphere();
	void calculateBounding();
    void render();
	void generateTangent(Vector3& v1, Vector3& v2, Vector3& st1, Vector3& st2, Vector3& tangent );
};

}

#endif