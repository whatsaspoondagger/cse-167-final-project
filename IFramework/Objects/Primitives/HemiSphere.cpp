#include "HemiSphere.h"

namespace IFramework {

HemiSphere::HemiSphere()
{
	radius = 1;
	slices = 1;
	stacks = 1;
	calculateBounding();
}

HemiSphere::HemiSphere(float r,int s, int t, float x, float y, float z):Geode(x,y,z)
{
	radius = r;
	slices = s;
	stacks = t;
	calculateBounding();
}

void HemiSphere::render()
{
  /// http://stackoverflow.com/questions/3276390/opengl-hemisphere-texture-mapping

  int i, j;
  int halfLats = stacks / 2;

  for(i = 0; i <= halfLats; i++) 
  {
        double lat0 = M_PI * (-0.5 + (double) (i - 1) / stacks);
        double z0 = sin(lat0)*radius;
        double zr0 = cos(lat0)*radius;

        double lat1 = M_PI * (-0.5 + (double) i / stacks);
        double z1 = sin(lat1)*radius;
        double zr1 = cos(lat1)*radius;

        glBegin(GL_QUAD_STRIP);
        for(j = 0; j <= slices; j++)
        {
			double lng = M_PI/2+(2 * M_PI * (double) (j - 1) / slices );
			double x = cos(lng);
			double y = sin(lng);

			double s1, s2, t;
			s1 = ((double) i) / halfLats;
			s2 = ((double) i + 1) / halfLats;
			t = ((double) j) / slices;

			glTexCoord2d(s2, t);
			glNormal3d(x * zr1, y * zr1, z1);
			glVertex3d(x * zr1, y * zr1, z1);

			glTexCoord2d(s1, t);
			glNormal3d(x * zr0, y * zr0, z0);
			glVertex3d(x * zr0, y * zr0, z0);


        }
        glEnd();
  }

  /*
	Base lid
  */
  glBegin(GL_TRIANGLE_FAN);
    glTexCoord2f(0.5, 0.5);
    glVertex3d(0,0,0);

	for(int i =0; i <= slices; i++){
		double angle = M_PI/2+(2 * M_PI * i / slices);
		double x = cos(angle)*radius;
		double y = sin(angle)*radius;

		double tx = x * 0.5 + 0.5;
		double ty = y * 0.5 + 0.5;

		glTexCoord2f(tx, ty);
		glVertex3d(x,y,0);
	}
  glEnd();
}

void HemiSphere::calculateBounding()
{
	box.corner = Vector3(-radius, - radius, 0);
	box.x = radius;
	box.y = radius;
	box.z = -radius;
}


}