#include "Sphere.h"

namespace IFramework {

Sphere::Sphere()
{
	radius = 1;
	slices = 10;
	stacks = 10;
	createSphere();
}

Sphere::Sphere(float r,int s, int t, float x,float y,float z):Geode(x,y,z)
{
	radius = r;
	slices = s;
	stacks = t;

	createSphere();
}

void Sphere::setNormalMapping(char* colorFile, char* normalMap){

	passingTangents = true;
	colorTexId = TextureManager::loadBMP(colorFile,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
	normalMapId = TextureManager::loadBMP(normalMap,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
}

void Sphere::render()
{
	//gluQuadricTexture(quad,1);
    //gluSphere(quad,radius, slices, stacks);
	if(passingTangents){
		glActiveTexture( GL_TEXTURE0 + 0 );
		glBindTexture( GL_TEXTURE_2D, colorTexId );

		glActiveTexture( GL_TEXTURE0 + 1);
		glBindTexture( GL_TEXTURE_2D, normalMapId );

		glUniform1i(glGetUniformLocation( shader->pid, "colorTex" ), 0 );
		glUniform1i(glGetUniformLocation( shader->pid, "normalMap" ), 1 );

		tangentLocation = glGetAttribLocationARB(shader->pid, "tangent");
	}

	glBegin( GL_QUADS );
		for( int i = 0; i < slices; i++ ) {
			for( int j = 0; j < stacks; j++ ) {
			
				int i1 = i * (slices+1) + j;
				int i2 = (i+1) * (slices+1) + j;
				int i3 = (i+1) * (slices+1) + j+1;
				int i4 = i * (slices+1) + j+1;

				if(passingTangents){
					glVertexAttrib3f( tangentLocation, tangents[i1][0], tangents[i1][1], tangents[i1][2] );
				}

				glNormal3f( normals[i1][0], normals[i1][1], normals[i1][2] );
				glTexCoord2f( texcoords[i1][0], texcoords[i1][1] );
				glVertex3f( vertices[i1][0],  vertices[i1][1],  vertices[i1][2] ); 

				if(passingTangents){
				    glVertexAttrib3f( tangentLocation, tangents[i2][0], tangents[i2][1], tangents[i2][2] );
				}
				glNormal3f( normals[i2][0], normals[i2][1], normals[i2][2] );
				glTexCoord2f( texcoords[i2][0], texcoords[i2][1] );
				glVertex3f( vertices[i2][0],  vertices[i2][1],  vertices[i2][2] ); 

				if(passingTangents){
				  glVertexAttrib3f( tangentLocation, tangents[i3][0], tangents[i3][1], tangents[i3][2] );
				}
				glNormal3f( normals[i3][0], normals[i3][1], normals[i3][2] );
				glTexCoord2f( texcoords[i3][0], texcoords[i3][1] );
				glVertex3f( vertices[i3][0],  vertices[i3][1],  vertices[i3][2] ); 

				if(passingTangents){
				  glVertexAttrib3f( tangentLocation, tangents[i4][0], tangents[i4][1], tangents[i4][2] );
				}
				glNormal3f( normals[i4][0], normals[i4][1], normals[i4][2] );
				glTexCoord2f( texcoords[i4][0], texcoords[i4][1] );
				glVertex3f( vertices[i4][0],  vertices[i4][1],  vertices[i4][2] ); 

			}
		}
	glEnd();

	//// This is the probably the most coolest utility function ever !
	//// Draw normals or tangents
	//Utils::drawNormalLines(vertices,tangents,(slices+1)*(stacks+1));
}

void Sphere::createSphere(){

	calculateBounding();

	passingTangents = false;

	normals = new double[(slices+1)*(stacks+1)][3];
	vertices = new double[(slices+1)*(stacks+1)][3];
	tangents = new double[(slices+1)*(stacks+1)][3];
	texcoords = new double[(slices+1)*(stacks+1)][2];

	float gammaStep = 2*M_PI / slices;
	float thetaStep = M_PI / slices;
	float gamma = 0, theta = 0;
	
	float s = 0, t = 0;
	float sStep = 1.0 / slices;
	float tStep = 1.0 / stacks;

	// Generate planet vertices
	for( int i = 0; i <= slices; i++ ) {

		gamma = i * gammaStep;
		s = i * sStep;
	
		for( int j = 0; j <= stacks; j++ ) {

			theta = j * thetaStep;
			t = j * tStep;

			Utils::spherePoint( vertices[i*(slices+1) + j] , radius, theta, gamma );
			
			Vector3 n = Vector3(vertices[i*(slices+1) + j][0],vertices[i*(slices+1) + j][1],vertices[i*(slices+1) + j][2]);

			n.normalize();

			normals[i*(slices+1) + j][0] = n.x;
			normals[i*(slices+1) + j][1] = n.y;
			normals[i*(slices+1) + j][2] = n.z;

			texcoords[i*(slices+1) + j][0] = -s;
			texcoords[i*(slices+1) + j][1] = -t;
		}
	}
	
	for( int i = 0; i <= slices; i++ ) {
		
		s = i * sStep;
		
		for( int j = 0; j <= stacks; j++ ) {
			
			t = j * tStep;
			
			Vector3 v1,v2,v3, v4;
			Vector3 tmp, tmp2;
			Vector3 st1, st2, tangent;
			int ind1 = i * (slices+1) + j;
			int ind2 = ((i+1) % slices) * (stacks+1) + j;

			v1 = Vector3(vertices[ind1][0],vertices[ind1][1],vertices[ind1][2]);
			v2 = Vector3(vertices[ind2][0],vertices[ind2][1],vertices[ind2][2]);

			st1.x = s;
			st1.y = t + tStep;
			st2.x = s + sStep;
			st2.y = t;

			if (j < slices) {

				int i1 = ((i+1) % slices) * (stacks+1) + j+1;
				int i2 = i * (slices+1) + j+1;
				v3 = Vector3(vertices[i1][0],vertices[i1][1],vertices[i1][2]);
				v4 = Vector3(vertices[i2][0],vertices[i2][1],vertices[i2][2]);
			}
			else {
				int i1 = ((i+1) % slices) * (stacks+1) + j-1;
				int i2 = i * (slices+1) + j-1;
				v3 = Vector3(vertices[i1][0],vertices[i1][1],vertices[i1][2]);
				v4 = Vector3(vertices[i2][0],vertices[i2][1],vertices[i2][2]);

				st1.x = s;
				st1.y = t - tStep;
			}

			tmp = v2 - v1;
			tmp2 = v4 - v1;
			
			generateTangent( tmp, tmp2, st1, st2, tangent );

			//tangent.print();
			tangents[ind1][0] = tangent.x;
			tangents[ind1][1] = tangent.y;
			tangents[ind1][2] = tangent.z;
		}
	}
	
}

void Sphere::generateTangent(Vector3& v1, Vector3& v2, Vector3& st1, Vector3& st2, Vector3& tangent ) {

	float coef = 1.0 / ( st1.x * st2.y - st2.x * st1.y );

	tangent.x = ( coef * ( ( v1.x * st2.y ) + v2.x * -st1.y ) );
	tangent.y = ( coef * ( ( v1.y * st2.y ) + v2.y * -st1.y ) );
	tangent.z = ( coef * ( ( v1.z * st2.y ) + v2.z * -st1.y ) );
	tangent.normalize();	
}

void Sphere::calculateBounding()
{
	box.corner = Vector3(-radius,-radius,-radius);
	box.x = radius;
	box.y = radius;
	box.z = radius;
}

}