#include "Plane.h"

namespace IFramework {

	Plane::Plane()
	{
		this->grid_size = 1;
		this->size = 1;

		calculateBounding();
		passingTangents = false;
	}

	Plane::Plane(float size,float grid_size,float x,float y,float z):Geode(x,y,z)
	{
		this->size = size;
		this->grid_size = grid_size;

		calculateBounding();
		passingTangents = false;
	}

	void Plane::render()
	{
		/*
		glBegin(GL_QUADS);
		// Front Face
		glNormal3f( 0.0f, 0.0f, 1.0f);                 
		glTexCoord2f(0.0f, 0.0f); 
		glVertex3f(-1.0f, -1.0f,  0.0f);

		glTexCoord2f(1.0f, 0.0f); 
		glVertex3f( 1.0f, -1.0f,  0.0f);

		glTexCoord2f(1.0f, 1.0f); 
		glVertex3f( 1.0f,  1.0f,  0.0f);

		glTexCoord2f(0.0f, 1.0f); 
		glVertex3f(-1.0f,  1.0f,  0.0f);
		glEnd();
		*/

		if(passingTangents){
			glActiveTexture( GL_TEXTURE0 + 0 );
			glBindTexture( GL_TEXTURE_2D, colorTexId );

			glActiveTexture( GL_TEXTURE0 + 1);
			glBindTexture( GL_TEXTURE_2D, normalMapId );

			glUniform1i(glGetUniformLocation( shader->pid, "colorTex" ), 0 );
			glUniform1i(glGetUniformLocation( shader->pid, "normalMap" ), 1 );

			tangentLocation = glGetAttribLocationARB(shader->pid, "tangent");
		}

		if(passingTangents){
			glVertexAttrib3f( tangentLocation, 0,0,1);
		}

		glNormal3f( 0.0f, 1.0f, 0.0f);
		glBegin(GL_QUADS); 

		int step = 2;

		for (int n = 0; n < size; ++n) {
			for (int m = 0; m < size; ++m) {

				glTexCoord2f((double)m/(double)(size) * step,(double)n/(double)(size) * step);
				//glTexCoord2f(0,0);
				//glTexCoord2f((double)m/(double)(step*grid_size-1),(double)n/(double)(step*grid_size-1));
				glVertex3f( m*grid_size, 0,n*grid_size);

				glTexCoord2f((double)m/(double)(size) * step,(double)(n+1)/(double)(size) * step);
				//glTexCoord2f(1,0);
				//glTexCoord2f((double)m/(double)(step*grid_size-1),(double)(n+1)/(double)(step*grid_size-1));
				glVertex3f(	m*grid_size, 0,(n+1)*grid_size);

				glTexCoord2f((double)(m+1)/(double)(size) * step,(double)(n+1)/(double)(size) * step);
				//glTexCoord2f(1,1);
				//glTexCoord2f((double)(m+1)/(double)(step*grid_size-1),(double)(n+1)/(double)(step*grid_size-1));
				glVertex3f( (m+1)*grid_size, 0,(n+1)*grid_size);

				glTexCoord2f((double)(m+1)/(double)(size) * step,(double)n/(double)(size) * step);
				//glTexCoord2f(0,1);
				//glTexCoord2f((double)(m+1)/(double)(step*grid_size-1),(double)n/(double)(step*grid_size-1));
				glVertex3f( (m+1)*grid_size, 0,(n)*grid_size);
			}
		}

		glEnd();
	}

	void Plane::calculateBounding()
	{
		box.corner = Vector3(0,0,0);
		box.x = size*grid_size;
		box.y = 1;
		box.z = size*grid_size;
	}


	void Plane::setNormalMapping(char* colorFile, char* normalMap){

		passingTangents = true;
		colorTexId = TextureManager::loadBMP(colorFile,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
		normalMapId = TextureManager::loadBMP(normalMap,GL_LINEAR,GL_LINEAR_MIPMAP_NEAREST);
	}

}