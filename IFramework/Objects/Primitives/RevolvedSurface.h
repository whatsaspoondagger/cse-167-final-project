#ifndef _RevolvedSurface_H_
#define _RevolvedSurface_H_

#include <Scenes\Geode.h>
#include <vector>
#include <Utils\Utils.h>
#include <Core\Math\Matrix4.h>
#include <Core\Geometry\Bezier.h>

namespace IFramework {

class RevolvedSurface : public Geode
{
  private:
	double (*vertices)[3];
	double (*fnormals)[3];
	Bezier* curve;
	double (*normals)[3];
	double (*texcoords)[2];

public:
	/// Variables
	double radius;
	double height;
	int sub_u;
	int sub_v;

	/// Methods
    RevolvedSurface(double inner_r,double outer_r,double h,int u, int t, double x,double y,double z);

	void calculateBounding();
    void render();

};

}

#endif