#include "Cylinder.h"

namespace IFramework {

Cylinder::Cylinder()
{
	b_radius = 1;
	t_radius = 1;
	height = 1;
	slices = 10;
	quad = gluNewQuadric();
	calculateBounding();
}

Cylinder::Cylinder(float b_r, float t_r, float h, int s, float x, float y, float z):Geode(x,y,z)
{
	b_radius = b_r;
	t_radius = t_r;
	height = h;
	slices = s;
	quad = gluNewQuadric();

	calculateBounding();
}

void Cylinder::render()
{
  /*
	Base lid
  */
  glBegin(GL_TRIANGLE_FAN);
	glNormal3f(0,0,-1);
    glTexCoord2f(0.5, 0.5);
    glVertex3d(0,0,0);

	for(int i =slices; i >= 0 ; --i){
		double angle = (2 * M_PI * i / slices)+M_PI/2;
		double x = cos(angle)*b_radius;
		double y = sin(angle)*b_radius;

		double tx = x * 0.5 + 0.5;
		double ty = y * 0.5 + 0.5;

		glNormal3f(0,0,-1);
		glTexCoord2f(tx, ty);
		glVertex3d(x,y,0);
	}
  glEnd();


  /*
	Top lid
  */
  glBegin(GL_TRIANGLE_FAN);
	glNormal3f(0,0,1);
    glTexCoord2f(0.5, 0.5);
    glVertex3d(0,0,height);

	for(int i =0; i <= slices; i++){
		double angle = (2 * M_PI * i / slices)+M_PI/2;
		double x = cos(angle)*t_radius;
		double y = sin(angle)*t_radius;

		double tx = x * 0.5 + 0.5;
		double ty = y * 0.5 + 0.5;

		glNormal3f(0,0,1);
		glTexCoord2f(tx, ty);
		glVertex3d(x,y,height);
	}
  glEnd();


  gluQuadricTexture(quad,1);
  gluCylinder(quad,b_radius,t_radius,height,slices,5);


}

void Cylinder::calculateBounding()
{
	float radius = (b_radius>t_radius) ? b_radius: t_radius;

	box.corner = Vector3(-radius,-radius, 0);
	box.x = radius;
	box.y = radius;
	box.z = height;
}


}