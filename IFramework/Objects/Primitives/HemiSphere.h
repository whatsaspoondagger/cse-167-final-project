#ifndef _HEMISPHERE_H_
#define _HEMISPHERE_H_

#include <Scenes\Geode.h>

namespace IFramework {

class HemiSphere : public Geode
{
  public:
    HemiSphere();
    HemiSphere(float radius,int slices, int stacks, float x,float y,float z);

	float radius;
	int slices,stacks;
	void calculateBounding();
    void render();
};

}

#endif