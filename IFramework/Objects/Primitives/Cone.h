#ifndef _CONE_H_
#define _CONE_H_

#include <Scenes\Geode.h>

namespace IFramework {

class Cone : public Geode
{
  private:
	GLUquadric *quad;

  public:
    Cone();
    Cone(float radius, float height , float slices, float x,float y,float z);

	float radius, height, slices;
	void calculateBounding();
    void render();
};

}

#endif