#include "RevolvedSurface.h"
#include <ctime>

namespace IFramework {

	RevolvedSurface::RevolvedSurface(double inner_r,double outer_r,double h,int _u, int _v, double x,double y,double z):Geode(x,y,z)
	{
		radius = outer_r;
		height = h;
		sub_u = _u+1;
		sub_v = _v;

		calculateBounding();

		vector<Vector3> ctrPoints;

		srand(static_cast<unsigned int>(time(0)));
		double y_1 = Utils::random(h/8, (3*h)/8 );
		double x_1 = Utils::random(inner_r,outer_r);
		double y_2 = Utils::random(5*h/8,7*h/8);
		double x_2 = Utils::random(inner_r,outer_r);
		
		ctrPoints.push_back(Vector3(inner_r,   0 , 0));
		ctrPoints.push_back(Vector3(x_1, y_1,0));
		ctrPoints.push_back(Vector3(x_2, y_2,0));
		ctrPoints.push_back(Vector3(inner_r,   h,  0));

		curve = new Bezier(ctrPoints,_u);

		double ratio = 360/double(sub_v);

		Matrix4 rotation;
		vertices = new double[sub_u*sub_v][3];
		normals = new double[sub_u*sub_v][3];
		fnormals = new double[sub_u*sub_v][3];
		texcoords = new double[sub_u*(sub_v+1)][2];
		
		Vector3* vert_temp;

		for(int n = 0; n <= sub_v; ++n){
			for(int m = 0; m < sub_u; ++m){
				vertices[n*sub_u+m][0] = 0; 
				vertices[n*sub_u+m][1] = 0; 
				vertices[n*sub_u+m][2] = 0; 

				normals[n*sub_u+m][0] = 0; 
				normals[n*sub_u+m][1] = 0; 
				normals[n*sub_u+m][2] = 0; 

				texcoords[n*sub_u+m][0] = (double)n/(double)(sub_v);
				texcoords[n*sub_u+m][1] = (double)m/((double)(sub_u-1));
			}
		}

		for(int n = 0; n < sub_v; ++n){
			rotation.rotateY(ratio*n);

			for(int m = 0; m < sub_u; ++m){

				vert_temp = curve->getVertices()->at(m);
				Vector4 temp2 = rotation * Vector4(vert_temp->x, vert_temp->y, vert_temp->z, 1);

				vertices[n*sub_u+m][0] = temp2.x;
				vertices[n*sub_u+m][1] = temp2.y;
				vertices[n*sub_u+m][2] = temp2.z;
			}
		}

		/// Compute normals for each face
		Vector3 a,b,c,normal;
		for(int n = 0; n < sub_v; ++n){
			for(int m = 0; m < sub_u-1; ++m){
				if(n>=sub_v-1){
					a = Vector3(vertices[m][0], vertices[m][1], vertices[m][2]);
					b = Vector3(vertices[n*sub_u+m][0], vertices[n*sub_u+m][1], vertices[n*sub_u+m][2]);
					c = Vector3(vertices[m+1][0], vertices[m+1][1], vertices[m+1][2]);
					normal = Vector3::normal(b,a,c);

					fnormals[n*sub_u + m][0] = normal.x;
					fnormals[n*sub_u + m][1] = normal.y;
					fnormals[n*sub_u + m][2] = normal.z;

					normals[m][0] += normal.x;
					normals[m][1] += normal.y;
					normals[m][2] += normal.z;

					normals[n*sub_u+m][0] += normal.x;
					normals[n*sub_u+m][1] += normal.y;
					normals[n*sub_u+m][2] += normal.z;

					normals[m+1][0] += normal.x;
					normals[m+1][1] += normal.y;
					normals[m+1][2] += normal.z;

					normals[n*sub_u+m+1][0] += normal.x;
					normals[n*sub_u+m+1][1] += normal.y;
					normals[n*sub_u+m+1][2] += normal.z;

				}else{
					a = Vector3(vertices[n*sub_u+m][0], vertices[n*sub_u+m][1], vertices[n*sub_u+m][2]);
					b = Vector3(vertices[n*sub_u+m+sub_u][0], vertices[n*sub_u+m+sub_u][1], vertices[n*sub_u+m+sub_u][2]);
					c = Vector3(vertices[n*sub_u+m+1][0], vertices[n*sub_u+m+1][1], vertices[n*sub_u+m+1][2]);
					normal = Vector3::normal(a,b,c);

					fnormals[n*sub_u + m][0] = normal.x;
					fnormals[n*sub_u + m][1] = normal.y;
					fnormals[n*sub_u + m][2] = normal.z;

					normals[n*sub_u+m+1][0] += normal.x;
					normals[n*sub_u+m+1][1] += normal.y;
					normals[n*sub_u+m+1][2] += normal.z;

					normals[n*sub_u+m][0] += normal.x;
					normals[n*sub_u+m][1] += normal.y;
					normals[n*sub_u+m][2] += normal.z;

					normals[n*sub_u+m+sub_u][0] += normal.x;
					normals[n*sub_u+m+sub_u][1] += normal.y;
					normals[n*sub_u+m+sub_u][2] += normal.z;

					normals[n*sub_u+m+sub_u+1][0] += normal.x;
					normals[n*sub_u+m+sub_u+1][1] += normal.y;
					normals[n*sub_u+m+sub_u+1][2] += normal.z;
				}
			}
		}


		for(int n = 0; n < sub_v; ++n){
			for(int m = 0; m < sub_u; ++m){
				normal = Vector3(normals[n*sub_u+m][0],normals[n*sub_u+m][1],normals[n*sub_u+m][2]);
				normal.normalize();

				normals[n*sub_u+m][0] = normal.x;
				normals[n*sub_u+m][1] = normal.y; 
				normals[n*sub_u+m][2] = normal.z; 
			}
		}
	}

	void RevolvedSurface::render()
	{
		//curve->render();

		glColor3f(1,1,1);
		glBegin(GL_QUADS);
		for(int n = 0; n < sub_v; ++n){
			for(int m = 0; m < sub_u-1; ++m){

				//glNormal3f(fnormals[n*sub_u+m][0], fnormals[n*sub_u+m][1],fnormals[n*sub_u+m][2]);
				if(n>=sub_v-1){
					
					glTexCoord2f(texcoords[n*sub_u+m][0], texcoords[n*sub_u+m][1]);
					glNormal3f(normals[n*sub_u+m][0], normals[n*sub_u+m][1], normals[n*sub_u+m][2]);
					glVertex3f(vertices[n*sub_u+m][0], vertices[n*sub_u+m][1], vertices[n*sub_u+m][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+sub_u][0], texcoords[n*sub_u+m+sub_u][1]);
					glNormal3f(normals[m][0], normals[m][1], normals[m][2]);
					glVertex3f(vertices[m][0], vertices[m][1], vertices[m][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+sub_u+1][0], texcoords[n*sub_u+m+sub_u+1][1]);
					glNormal3f(normals[m+1][0], normals[m+1][1], normals[m+1][2]);
					glVertex3f(vertices[m+1][0], vertices[m+1][1], vertices[m+1][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+1][0], texcoords[n*sub_u+m+1][1]);
					glNormal3f(normals[n*sub_u+m+1][0], normals[n*sub_u+m+1][1], normals[n*sub_u+m+1][2]);
					glVertex3f(vertices[n*sub_u+m+1][0], vertices[n*sub_u+m+1][1], vertices[n*sub_u+m+1][2]);

				}else{
					glTexCoord2f(texcoords[n*sub_u+m][0], texcoords[n*sub_u+m][1]);
					glNormal3f(normals[n*sub_u+m][0], normals[n*sub_u+m][1], normals[n*sub_u+m][2]);
					glVertex3f(vertices[n*sub_u+m][0], vertices[n*sub_u+m][1], vertices[n*sub_u+m][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+sub_u][0], texcoords[n*sub_u+m+sub_u][1]);
					glNormal3f(normals[n*sub_u+m+sub_u][0], normals[n*sub_u+m+sub_u][1], normals[n*sub_u+m+sub_u][2]);
					glVertex3f(vertices[n*sub_u+m+sub_u][0], vertices[n*sub_u+m+sub_u][1], vertices[n*sub_u+m+sub_u][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+sub_u+1][0], texcoords[n*sub_u+m+sub_u+1][1]);
					glNormal3f(normals[n*sub_u+m+sub_u+1][0], normals[n*sub_u+m+sub_u+1][1], normals[n*sub_u+m+sub_u+1][2]);
					glVertex3f(vertices[n*sub_u+m+sub_u+1][0], vertices[n*sub_u+m+sub_u+1][1], vertices[n*sub_u+m+sub_u+1][2]);
					
					glTexCoord2f(texcoords[n*sub_u+m+1][0], texcoords[n*sub_u+m+1][1]);
					glNormal3f(normals[n*sub_u+m+1][0], normals[n*sub_u+m+1][1], normals[n*sub_u+m+1][2]);
					glVertex3f(vertices[n*sub_u+m+1][0], vertices[n*sub_u+m+1][1], vertices[n*sub_u+m+1][2]);
				}
			}
		}

		glEnd();
	}

	void RevolvedSurface::calculateBounding()
	{
		box.corner = Vector3(- radius, - radius, 0 );
		box.x = radius;
		box.y = radius;
		box.z = height;
	}

}