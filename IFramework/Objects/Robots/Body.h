#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <ctime>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Sphere.h>
#include <Objects\Primitives\RevolvedSurface.h>
#include <Objects\Primitives\HemiSphere.h>

namespace IFramework {

class Body: public Group
{
public:	
	Vector3 head_pos, r_leg_pos, l_leg_pos, r_arm_pos, l_arm_pos, back_pos;

	Body(double scale,int type);
};

}

#endif