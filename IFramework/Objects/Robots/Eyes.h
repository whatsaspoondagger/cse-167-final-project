#ifndef _EYES_H_
#define _EYES_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Sphere.h>
#include <Objects\Primitives\Cube.h>

namespace IFramework {

class Eyes: public Group
{
	public:
		Eyes(double scale,int type);
};

}

#endif