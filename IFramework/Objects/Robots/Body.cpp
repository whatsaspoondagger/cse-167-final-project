#include "Body.h"
#include <iostream>

using namespace std;

namespace IFramework {

/*
	Types = 2
*/
Body::Body(double scale,int type){
	
	double r = scale*1.3;
	int slices = 20;
	double d = r/sqrt(2.0);
	Geode* geometry;

	double h = r*2;
	double b_r = r*0.8;
	double t_r = r;

	switch(type){
	case 0:
		geometry = new Sphere(r,slices,slices,0,0,0);
		
		head_pos = Vector3(0,r,0);
		
		l_arm_pos = Vector3(-d,d,0);
		r_arm_pos = Vector3(d,d,0);

		l_leg_pos = Vector3(-d+r/4,-d,0);
		r_leg_pos = Vector3(d-r/4,-d,0);

		back_pos = Vector3(0,h/2,-r);
		this->addChild(geometry);
	break;
	case 1:

		HemiSphere* hs = new HemiSphere(b_r,slices,slices,0,0,0);
		hs->transform.setAngle(Vector3(1,0,0),-90);
		geometry = new Cylinder(b_r,t_r,h,slices,0,0,0);

		geometry->transform.setAngle(Vector3(1,0,0),-90);
		head_pos = Vector3(0,h,0);
		l_arm_pos = Vector3(-t_r,h,0);
		r_arm_pos = Vector3(t_r,h,0);
		l_leg_pos = Vector3(-b_r,0,0);
		r_leg_pos = Vector3(b_r,0,0);
		
		back_pos = Vector3(0,h/2,-t_r);
		this->addChild(geometry);
		this->addChild(hs);
	break;
	}

}
}