#include "Back.h"
#include <iostream>

using namespace std;

namespace IFramework {

/*
	Types = 2
*/
	Back::Back(double scale, int type){

		double r = scale*0.2;
		double w = scale;
		double h = scale*2;
		double t = scale*0.5;
		int slices = 10;

		Cube* back;
		switch(type){
			case 0:
				back = new Cube(w,h,t,0,0,0);
				this->addChild(back);
			break;
			case 1:
				back  = new Cube(w,h/4,t,0,-h/2,0);
				this->addChild(back);

				Cylinder* c1 = new Cylinder(r,r,h,slices,w/2,-h/2,0);
				Cylinder* c2 = new Cylinder(r,r,h,slices,-w/2,-h/2,0);
				c1->transform.setAngle(Vector3(1,0,0),-90);
				c2->transform.setAngle(Vector3(1,0,0),-90);

				c1->transform.move(Vector3(0,-3,0),Vector3(0,1,0),50);
				c2->transform.move(Vector3(0,-3,0),Vector3(0,1,0),50);
				
				this->addChild(c1);
				this->addChild(c2);
			break;

		}
	}
}