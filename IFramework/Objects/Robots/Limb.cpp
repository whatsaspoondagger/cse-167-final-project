#include "Limb.h"
#include <iostream>

using namespace std;

/*
	Types: 2
*/
namespace IFramework {

	Limb::Limb(double scale, int type){

		double r = scale;
		double h = scale*5;
		double slices = 10;

		Geode* geometry;

		switch(type){
		case 0:
			geometry = new RevolvedSurface(r,r*2.5,h,slices,slices,0,0,0); 
			
			end = Vector3(0,h,0);
			start = Vector3(0,0,0);
			this->addChild(geometry);
		break;
		case 1:
			geometry = new Cone(r,h,slices,0,0,0); 
			geometry->transform.setAngle(Vector3(1,0,0),-90);

			end = Vector3(0,h,0);
			start = Vector3(0,0,0);
			this->addChild(geometry);
		break;		
		}
		
	}
}