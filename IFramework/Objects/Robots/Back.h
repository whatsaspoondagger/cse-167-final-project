#ifndef _BACK_H_
#define _BACK_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <ctime>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Cone.h>
#include <Objects\Primitives\Cube.h>

namespace IFramework {

class Back: public Group
{
	public:	
		Back(double scale, int type);
};

}

#endif