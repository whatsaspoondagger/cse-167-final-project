#include "Eyes.h"
#include <iostream>

using namespace std;

/*
	Types:1
*/
namespace IFramework {
	Eyes::Eyes(double scale,int type){

		Geode* l_eye,* r_eye;
		double r = scale*0.8;
		double h = scale/4;
		double slices = 10;

		switch(type){
		case 0:
		l_eye = new Cylinder(r,r,h,slices,0,0,0);
		r_eye = new Cylinder(r,r,h,slices,0,0,0);
		l_eye->transform.setPosition(-r-r/3,0,0);
		r_eye->transform.setPosition(r+r/3,0,0);

		this->addChild(l_eye);
		this->addChild(r_eye);
		break;
		}

	}
}