#ifndef _HEAD_H_
#define _HEAD_H_

#include <Scenes\Geode.h>
#include <Core\Math\Vector3.h>
#include <math.h>
#include <Scenes\Group.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Sphere.h>
#include <Objects\Primitives\Cube.h>
#include <Objects\Primitives\HemiSphere.h>

namespace IFramework {

class Head: public Group
{
	public:
		Vector3 eyes_pos, decorator_pos;
		Head(double scale,int type);
};

}

#endif