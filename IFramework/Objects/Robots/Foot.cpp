#include "Foot.h"
#include <iostream>

using namespace std;

namespace IFramework {
/*
	Types = 1
*/
	Foot::Foot(double scale){

		double w = scale*1.5;
		double h = scale*2;
		double t = scale*0.6;
		int slices = 10;

		int numb_toes = 3;
		double toe_radius = scale*0.6;
		double toe_height = scale*1.3;

		Cube* palm = new Cube(w,h,t,0,0,0);
		palm->transform.setAngle(Vector3(1,0,0),90);
		this->addChild(palm);

		for(int i=0; i < numb_toes ; ++i){
			Cylinder* toe = new Cylinder(toe_radius,toe_radius,toe_height,slices,i*(toe_radius*2)-2*toe_radius,0,-h);//;);
			toe->transform.setAngle(Vector3(1,0,0),180);
			this->addChild(toe);
		}
		
	}
}