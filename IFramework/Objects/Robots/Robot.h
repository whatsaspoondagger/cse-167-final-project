#ifndef _ROBOT_H_
#define _ROBOT_H_

#include <Scenes\Geode.h>
#include <Scenes\Group.h>
#include <Objects\Robots\Body.h>
#include <Objects\Robots\Limb.h>
#include <Objects\Robots\Joint.h>
#include <Objects\Robots\Shoulder.h>
#include <Objects\Robots\Neck.h>

#include <Objects\Robots\Head.h>
#include <Objects\Robots\Eyes.h>
#include <Objects\Robots\Head_Decorator.h>
#include <Objects\Robots\Hand.h>
#include <Objects\Robots\Foot.h>
#include <Objects\Robots\Back.h>

namespace IFramework {

class Robot: public Group
{
	private:
		
	public:
		
		Robot();
};

}

#endif