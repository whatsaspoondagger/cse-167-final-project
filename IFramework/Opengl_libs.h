#ifndef _OpenGLLIBS_H_
#define _OpenGLLIBS_H_

#include <stdio.h>
#include <stdlib.h>

#pragma comment(lib, "glew32.lib")

#include <GL/glew.h>
#include <GL/glut.h>

#endif
