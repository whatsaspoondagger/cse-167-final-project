#pragma once

#include <Opengl_libs.h>
#include <vector>
#include <Utils\pngLoader.h>
#include <iostream>
#include <Utils\bmpLoader.h>

class TextureManager
{
  private:
	static GLuint currentIndex;
	static GLuint* textures;
  public:
	static void init();
    static GLuint loadPNG(char* filename, GLint  gl_mag, GLint gl_min);
	static GLuint loadBMP(char* filename, GLint  gl_mag, GLint gl_min);
};

