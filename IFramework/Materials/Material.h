#ifndef MATERIAL_H
#define	MATERIAL_H

#include <iostream>
#include <GL/glut.h>

using namespace std;

namespace IFramework {

class Material {

	private:
	GLfloat color[4];
	GLfloat ambient[4];
	GLfloat specular[4];
	GLfloat diffuse[4];
	GLfloat shininess[1];

	public:
	/// Variables
	const static GLfloat WHITE[4];
	const static GLfloat BLACK[4];
	const static GLfloat RED[4];
	const static GLfloat GREEN[4];
	const static GLfloat BLUE[4];

	/// Methods
	Material();
	void setColor(float r,float g,float b,float a);
	void setAmbient(float r,float g,float b);
	void setSpecular(float r,float g,float b);
	void setDiffuse(float r,float g, float b);
	void setShininess(float s);
	void render();
};

}

#endif

