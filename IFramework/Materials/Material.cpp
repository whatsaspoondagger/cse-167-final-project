#include "Material.h"

namespace IFramework {

	const static GLfloat WHITE[4] = {1,1,1,1};
	const static GLfloat BLACK[4] = {0,0,0,1};
	const static GLfloat RED[4] = {1,0,0,1};
	const static GLfloat GREEN[4] = {0,1,0,1};
	const static GLfloat BLUE[4] = {0,1,0,1};

Material::Material(){

   color[0] = 0.8;
   color[1] = 0.8;
   color[2] = 0.8;
   color[3] = 1;

   ambient[0] = 0.1;
   ambient[1] = 0.1;
   ambient[2] = 0.1;
   ambient[3] = 0.1;

   specular[0] = 1;
   specular[1] = 1;
   specular[2] = 1;
   specular[3] = 1;

   diffuse[0] = 0.8;
   diffuse[1] = 0.8;
   diffuse[2] = 0.8;
   diffuse[3] = 0.8;

   shininess[0] = 10;

}

void Material::setColor(float r, float g, float b,float a){
   color[0] = r;
   color[1] = g;
   color[2] = b;
   color[3] = a;
}

void Material::setAmbient(float r, float g, float b){
   ambient[0] = r;
   ambient[1] = g;
   ambient[2] = b;
   ambient[3] = 1;
}

void Material::setDiffuse(float r, float g, float b){
   diffuse[0] = r;
   diffuse[1] = g;
   diffuse[2] = b;
   diffuse[3] = 1;
}

void Material::setSpecular(float r, float g, float b){
   specular[0] = r;
   specular[1] = g;
   specular[2] = b;
   specular[3] = 1;
}

void Material::setShininess(float s){
   shininess[0] = s;
}

void Material::render(){
   glColor4f(color[0],color[1],color[2],color[3]);   
   glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
   glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
   glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
}

}