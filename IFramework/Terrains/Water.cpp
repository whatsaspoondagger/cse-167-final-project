#include "Water.h"

namespace IFramework {

	Water::Water(int size,int grid_size, double x,double y,double z,GLuint colorMap, GLuint normalMap):Geode(x,y,z)
	{
		this->size = size;
		this->grid_size = grid_size;
		water_pos = 0;
		calculateBounding();

		colorTexId = colorMap;
		normalMapId = normalMap;
	}

	void Water::render(){

		water_pos += 0.001;

		glActiveTexture( GL_TEXTURE0 + 0 );
		glBindTexture( GL_TEXTURE_2D, colorTexId );

		glActiveTexture( GL_TEXTURE0 + 1);
		glBindTexture( GL_TEXTURE_2D, normalMapId );

		glUniform1i(glGetUniformLocation( shader->pid, "colorTex" ), 0 );
		glUniform1i(glGetUniformLocation( shader->pid, "normalMap" ), 1 );

		tangentLocation = glGetAttribLocationARB(shader->pid, "tangent");
		
		glUniform1f(glGetUniformLocation( shader->pid, "water_pos" ), water_pos );

		glVertexAttrib3f( tangentLocation, 0,0,1);
		glNormal3f( 0.0f, 1.0f, 0.0f);
		glBegin(GL_QUADS); 

		int step = 2;

		for (int n = 0; n < size; ++n) {
			for (int m = 0; m < size; ++m) {

				glTexCoord2f((double)m/(double)(size) * step,(double)n/(double)(size) * step);
				//glTexCoord2f(0,0);
				//glTexCoord2f((double)m/(double)(step*grid_size-1),(double)n/(double)(step*grid_size-1));
				glVertex3f( m*grid_size, 0,n*grid_size);

				glTexCoord2f((double)m/(double)(size) * step,(double)(n+1)/(double)(size) * step);
				//glTexCoord2f(1,0);
				//glTexCoord2f((double)m/(double)(step*grid_size-1),(double)(n+1)/(double)(step*grid_size-1));
				glVertex3f(	m*grid_size, 0,(n+1)*grid_size);

				glTexCoord2f((double)(m+1)/(double)(size) * step,(double)(n+1)/(double)(size) * step);
				//glTexCoord2f(1,1);
				//glTexCoord2f((double)(m+1)/(double)(step*grid_size-1),(double)(n+1)/(double)(step*grid_size-1));
				glVertex3f( (m+1)*grid_size, 0,(n+1)*grid_size);

				glTexCoord2f((double)(m+1)/(double)(size) * step,(double)n/(double)(size) * step);
				//glTexCoord2f(0,1);
				//glTexCoord2f((double)(m+1)/(double)(step*grid_size-1),(double)n/(double)(step*grid_size-1));
				glVertex3f( (m+1)*grid_size, 0,(n)*grid_size);
			}
		}

		glEnd();
	}
        void Water::calculateBounding()
        {
                box.corner = Vector3(-size*grid_size,0,-size*grid_size);
                box.x = size*grid_size*3;
                box.y = 10;
                box.z = size*grid_size*3;
        }

}