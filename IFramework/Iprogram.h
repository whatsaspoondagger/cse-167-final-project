#ifndef _IPROGRAM_H_
#define _IPROGRAM_H_

#include <Shaders\Shader.h>
#include <Shaders\ShaderManager.h>

#include <Scenes\Group.h>
#include <Objects\Primitives\Plane.h>
#include <Objects\Primitives\HemiSphere.h>
#include <Objects\Primitives\Cylinder.h>
#include <Objects\Primitives\Cone.h>
#include <Objects\Primitives\Sphere.h>
#include <Objects\Primitives\Cube.h>
#include <Objects\Primitives\RevolvedSurface.h>
#include <Objects\Object3d.h>
#include <Objects\Lights\DirectionalLight.h>
#include <Objects\Lights\SpotLight.h>
#include <Objects\Lights\PointLight.h>

#include <Objects\Robots\Robot.h>
#include <Textures\TextureManager.h>
#include <Cameras\Camera.h>
#include <Terrains\Terrain.h>
#include <Terrains\Water.h>
#include <Terrains\SkyBox.h>

#include <GL/glut.h>

namespace IFramework {

class Iprogram
{
  private:
  //// Variables
	static int frame, timebase;
	static Matrix4 worldMatrix;
	static void calcualteFPS();
	static char title[80];

  //// Methods
    static void reshapeCallback(int, int);
    static void displayCallback(void);
    static void idleCallback(void);
	static void mouse(int x, int y);
	static void keyDown (unsigned char key, int x, int y);
	static void keyUp (unsigned char key, int x, int y);

  public:
  //// Variables
	static int TIME;
	static SkyBox sky;
	static int width,height;
	static Group world;
	static bool useSkyBox;
	static bool useWriteFrame;
	static void (*loopFunc)(void);
	static void (*keyFunc)(unsigned char);
  //// Methods
	static void init(int, char *[],char*,int,int);
	static void start();
};

}

#endif