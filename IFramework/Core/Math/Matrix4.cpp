#include "Matrix4.h"
#include <Utils\Utils.h>

using namespace std;

namespace IFramework {

Matrix4::Matrix4()
{
  for (int i=0; i<4; ++i)
  {
    for (int j=0; j<4; ++j)
    {
      m[i][j] = 0;
    }
  }
}

//column major
/*

m00 m10 m20 m30
m01
m02         m32
m03     m23 m33  

*/
Matrix4::Matrix4(
  double m00, double m01, double m02, double m03,
  double m10, double m11, double m12, double m13,
  double m20, double m21, double m22, double m23,
  double m30, double m31, double m32, double m33 )
{
  m[0][0] = m00;
  m[0][1] = m10;
  m[0][2] = m20;
  m[0][3] = m30;
  m[1][0] = m01;
  m[1][1] = m11;
  m[1][2] = m21;
  m[1][3] = m31;
  m[2][0] = m02;
  m[2][1] = m12;
  m[2][2] = m22;
  m[2][3] = m32;
  m[3][0] = m03;
  m[3][1] = m13;
  m[3][2] = m23;
  m[3][3] = m33;
}

double* Matrix4::getPointer()
{
  return &m[0][0];
}

void Matrix4::identity()
{
  double ident[4][4]={{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};
  for (int i=0; i<4; ++i)
  {
    for (int j=0; j<4; ++j)
    {
      m[i][j] = ident[i][j];
    }
  }
}

// Matrix multiplication
Matrix4 Matrix4::operator*(Matrix4 &a){

    Matrix4 res;

    for (int j = 0; j < 4; ++j) {
        for (int i = 0; i < 4; ++i) {
            for (int k = 0; k < 4; ++k) {
                res.m[j][i] += m[k][i] * a.m[j][k];
            }
        }
    }

    return res;
}

void Matrix4::operator*=(Matrix4 &a) {
        *this = *this * a;
}

Vector4 Matrix4::operator*(Vector4 &a){

    double tmp[4] = {0, 0, 0, 0};

    for (int i = 0; i < 4; ++i) {
        for (int k = 0; k < 4; ++k) {
            tmp[i] += m[k][i] * a[k];
        }
    }

    Vector4 res(tmp[0], tmp[1], tmp[2], tmp[3]);
    return res;
}

void Matrix4::set(int row, int col, int value){
	m[row][col] = value;
}

void Matrix4::rotateX(double angle)
{
	angle = Utils::degreeToRad(angle);

    double c = std::cos(angle);
    double s = std::sin(angle);
    *this = Matrix4(1, 0, 0, 0,
                    0, c, -s,0,
                    0, s, c, 0,
                    0, 0, 0, 1);
}

void Matrix4::rotateY(double angle)
{
	angle = Utils::degreeToRad(angle);

    double c = std::cos(angle);
    double s = std::sin(angle);
    *this = Matrix4(c, 0, s, 0,
                    0, 1, 0, 0,
                    -s,0, c, 0,
                    0, 0, 0, 1);
}

void Matrix4::rotateZ(double angle)
{
	angle = Utils::degreeToRad(angle);

    double c = std::cos(angle);
    double s = std::sin(angle);
    *this = Matrix4(c,-s, 0, 0,
                    s, c, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1);
}

void Matrix4::rotate(double angle, double x, double y, double z) {
    double c = std::cos(angle);
    double s = std::sin(angle);
    *this = Matrix4(x*x+c*(1-x*x), x*y*(1-c)-z*s, x*z*(1-c)+y*s, 0,
                    x*y*(1-c)+z*s, y*y+c*(1-y*y), y*z*(1-c)-x*s, 0,
                    x*z*(1-c)-y*s, y*z*(1-c)+x*s, z*z+c*(1-z*z), 0,
                    0,             0,             0, 1);
}

void Matrix4::scale(double sx, double sy, double sz) {
    *this = Matrix4();
    m[0][0] = sx;
    m[1][1] = sy;
    m[2][2] = sz;
    m[3][3] = 1;
}

void Matrix4::translate(double tx, double ty, double tz) {
    *this = Matrix4(1, 0, 0, tx,
                    0, 1, 0, ty,
                    0, 0, 1, tz,
                    0, 0, 0, 1);
}

void Matrix4::print() {
    for (int i = 0; i < 4; ++i) {
        std::printf("%f %f %f %f\n", m[0][i], m[1][i], m[2][i], m[3][i]);
    }
}

Matrix4 Matrix4::clone() {
    Matrix4 mat = Matrix4();
     
 	for(int i=0; i<4; ++i){
		for(int j=0; j<4; ++j){
			 mat.m[i][j] = m[i][j]; 
		}
	}
         
    return mat;
}


void Matrix4::transpose() {
    Matrix4 b = this->clone();
    
 	for(int i=0; i<4; ++i){
		for(int j=0; j<4; ++j){
			 m[i][j] = b.m[j][i]; 
		}
	}
}


}