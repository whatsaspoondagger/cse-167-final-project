#include "Vector4.h"
#include <math.h>
#include "Vector3.h"

namespace IFramework {

/* Constructors */

Vector4::Vector4() {
	w=x=y=z=0;
}
Vector4::Vector4(double x0,double y0,double z0,double w0){
	w=w0; x=x0; y=y0; z=z0;
}

void Vector4::set( double x0,double y0,double z0,double w0) {
	w=w0; x=x0; y=y0; z=z0;
}

double Vector4::get(int i) {
	switch (i){
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: 
		//should throw exception
		return 0;
	}
}

Vector4::Vector4(Vector3&a)
{
  x=a.x; y=a.y; z=a.z; w=1;
}

Vector4 Vector4::operator+(const Vector4&a)const {
	Vector4 b =  Vector4(w+a.w, x+a.x, y+a.y, z+a.z);
	return b;
}

Vector4 Vector4::operator-(const Vector4&a) const {
	Vector4 b =  Vector4(w-a.w, x-a.x, y-a.y, z-a.z);
	return b;
}

double Vector4::operator[](int i) {
	switch (i){
	case 0: return x;
	case 1: return y;
	case 2: return z;
	case 3: return w;
	default: 
		//should throw exception
		return 0;
	}
}

Vector4 Vector4::operator*(double a) {
	return Vector4(x*a,y*a,z*a,w*a);
}

Vector4 Vector4::operator/(double a){
	if(a==0.0)
	{
		cerr<<"Divide by 0 error"<<endl;
	}
	double oneOverA=1.0f/a;
    
	return Vector4(x*oneOverA,y*oneOverA,z*oneOverA,w*oneOverA);
}

double Vector4::dot(Vector4& a) {
	return w*a.w+x*a.x+y*a.y+z*a.z;
}

double Vector4::length() {
	return (double)sqrt(w*w+x*x+y*y+z*z);
}

void Vector4::normalize() { 
	double length=this->length();
	if(length>0.0f)
	{
		*this=*this/length;
	}	
}

void Vector4::dehomogenize()
{
	if(w>0.0f){
		x=x/w;
		y=y/w;
		z=z/w;
		w=1;
	}else{
		std::cout<<"dehomogenize error divide by 0"<<std::endl;
	}
}

void Vector4::print(){
	cout<<x<<" "<<y<<" "<<z<<" "<<w<<endl;
}

}