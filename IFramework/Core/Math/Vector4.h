#ifndef _VECTOR4_
#define _VECTOR4_

#include <iostream>
#include "Vector3.h"

namespace IFramework {

class Vector4 {
public:
    double w,x,y,z;

	/// Methods
	Vector4();
	Vector4(double x0,double y0,double z0,double w0);
    Vector4(Vector3&);
	void set( double x0,double y0,double z0,double w0);
	double get(int);

	/// Operators
	Vector4 operator+(const Vector4& ) const;
	double operator[](int);
	Vector4 operator-(const Vector4&)const;
    Vector4 operator*(double a);
	Vector4 operator/(double a);
	
	/// Methods
	double dot(Vector4& a);
	double length();
	void normalize();
	void dehomogenize();
	void print();
};

}
#endif