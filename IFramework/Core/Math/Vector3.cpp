#include "Vector3.h"

namespace IFramework {

/*
	Contructors
*/
Vector3::Vector3() {
	x=y=z=0;
}

Vector3::Vector3(double x0,double y0,double z0)	{
	x=x0; y=y0; z=z0;
}

Vector3::Vector3(const Vector3 &a):x(a.x),y(a.y),z(a.z) {
}

void Vector3::set(double x0,double y0,double z0) {
	x=x0; y=y0; z=z0;
}

double Vector3::get(int a) {
	
	switch (a) {
	case 0:
		return x;
		break;
	case 1:
		return y;
		break;
	case 2:
		return z;
		break;
	default:
		cout<<"Can't find a vector component"<<endl;
		return 0;
	}
	
}

/*
	Operators
*/

Vector3 & Vector3::operator=(const Vector3&a){
	x=a.x; y=a.y; z=a.z;
	return *this;
}

Vector3 Vector3::operator+(Vector3&a){
	return Vector3(x+a.x,y+a.y,z+a.z);
}

void Vector3::operator+=(Vector3&a){
	*this = *this+a;
}

Vector3 Vector3::operator-(Vector3&a){
	Vector3 b =  Vector3(x-a.x,y-a.y,z-a.z);
	return b;
}

void Vector3::operator-=(Vector3&a){
	*this = *this-a;
}

double Vector3::operator[](int i) {
	switch (i){
	case 0: return x;
	case 1: return y;
	case 2: return z;
	default: 
		cout<<"Can't find a vector component"<<endl;
		return 0;
	}
}

bool Vector3::operator==(const Vector3&a){ 
	return x==a.x&&y==a.y&&z==a.z;
}

bool Vector3::operator!=(const Vector3&a) {
	return x!=a.x||y!=a.y||z!=a.z;
}

Vector3 Vector3::operator*(double a)
{
	return Vector3(a*x,a*y,a*z);
}

void Vector3::operator*=(double a)
{
	*this = *this*a;
}

Vector3 Vector3::operator*(Vector3&a)
{
	return Vector3(y*a.z-z*a.y,z*a.x-x*a.z,x*a.y-y*a.x);
}

Vector3 Vector3::operator/(double s) {
	return Vector3(x/s,y/s,z/s);
}

/*
	Methods
*/
Vector3 Vector3::add(Vector3& a) {
	x=x+a.x; y=y+a.y; z=z+a.z; 
	return *this;
}

Vector3 Vector3::sub(Vector3& a) {
	x=x-a.x; y=y-a.y; z=z-a.z; 
	return *this;
}

Vector3 Vector3::mult(double s) {
	x=x*s; y=y*s; z=z*s; 
	return *this;
}

double Vector3::dot(Vector3& a) {
	return x*a.x+y*a.y+z*a.z;
}

Vector3 Vector3::div(double s) {
	if(s!=0){
		x=x/s; y=y/s; z=z/s; 
	}
	return *this;
}

Vector3 Vector3::cross(Vector3& a){
	Vector3 r;
	r.x= y*a.z-z*a.y;
	r.y= z*a.x-x*a.z;
	r.z= x*a.y-y*a.x;

	return r;
}

void Vector3::cross(Vector3& a,Vector3& b){

	x= a.y*b.z-a.z*b.y;
	y= a.z*b.x-a.x*b.z;
	z= a.x*b.y-a.y*b.x;
}

double Vector3::length() {
	return (double)sqrt(x*x+y*y+z*z);
}

void Vector3::normalize() { 
	div(length()); 
}

Vector3 Vector3::clone(){
	Vector3 r(x,y,z);
	return r;
}

double Vector3::angle(Vector3&a)
{
	return acos(this->dot(a)/(this->length()*a.length()));
}

void Vector3::limit(double max){
	if(this->length()>max){
		this->normalize();
		abs(max);
		x*=max;
		y*=max;
		z*=max;
	}
}

void Vector3::print(){
	cout<<x<<" "<<y<<" "<<z<<endl;
}

Vector3 Vector3::normal(Vector3 a, Vector3 b, Vector3 c) {
    Vector3 n;
    n.cross(a - c, b - c);
    n.normalize();
    return n;
}

}