/*Boid object class
* Matt Wetmore
* Changelog
* ---------
* 12/14/09: Started work
* 12/16/09: Revised code to work with forces instead of heading. First steering algorithm implemented
* 12/18/09: Arrival steering behavior
* 12/20/09: Alignment added NEED TO FIX COHESION
* 1/6/10: Finished algorithms. Time to clean up the code!
*/

#include <stdlib.h>
#include <Core\Math\Vector3.h>
#include <vector>
#include <GL/glut.h>

namespace IFramework{

static int height = 600;
static int width = 600;

class Boid
{
private:

public:
  //fields
  Vector3 pos,vel,acc,ali,coh,sep; //pos, velocity, and acceleration in a vector datatype
  double neighborhoodRadius; //radius in which it looks for fellow boids
  double maxSpeed; //maximum magnitude for the velocity vector
  double maxSteerForce; //maximum magnitude of the steering vector
  double h; //hue
  double sc; //scale factor for the render of the boid
  double flap;
  double t;
  bool avoidWalls;
   


  Boid::Boid(){
	  maxSpeed = 4;
	  maxSteerForce = .1;
	  sc=3;
	  flap = 0;
	  t=0;
	  avoidWalls = false;
  }

  //constructors
  Boid::Boid(Vector3 inPos)
  {
    pos = inPos;
    vel = Vector3(rand()%2-1,rand()%2-1,rand()%2-1);
    acc = Vector3(0,0,0);
    neighborhoodRadius = 400;

	  maxSpeed = 4;
	  maxSteerForce = .1;
	  sc=3;
	  flap = 0;
	  t=0;
	  avoidWalls = true;
  }

  Boid(Vector3 inPos,Vector3 inVel,double r)
  {
    pos = inPos;
    vel = inVel;
    acc = Vector3(0,0,0);
    neighborhoodRadius = r;
  }
   
  void run(vector<Boid*> bl)
  {
    t+=.1;
    flap = 10*sin(t);
    //acc.add(steer(new Vector3(mouseX,mouseY,300),true));
    //acc.add(new Vector3(0,.05,0));

    if(avoidWalls)
    {
	  /*
      acc.add(Vector3.mult(avoid(new Vector3(pos.x,height,pos.z),true),5));
      acc.add(Vector3.mult(avoid(new Vector3(pos.x,0,pos.z),true),5));
      acc.add(Vector3.mult(avoid(new Vector3(width,pos.y,pos.z),true),5));
      acc.add(Vector3.mult(avoid(new Vector3(0,pos.y,pos.z),true),5));
      acc.add(Vector3.mult(avoid(new Vector3(pos.x,pos.y,300),true),5));
      acc.add(Vector3.mult(avoid(new Vector3(pos.x,pos.y,900),true),5));
	  */
	  acc.add(avoid(Vector3(pos.x,height,pos.z),true).mult(5));
      acc.add(avoid(Vector3(pos.x,0,pos.z),true).mult(5));
      acc.add(avoid(Vector3(width,pos.y,pos.z),true).mult(5));
      acc.add(avoid(Vector3(0,pos.y,pos.z),true).mult(5));
      acc.add(avoid(Vector3(pos.x,pos.y,300),true).mult(5));
      acc.add(avoid(Vector3(pos.x,pos.y,900),true).mult(5));

    }
    
	flock(bl);
    move();
    checkBounds();
    render();
  }
   
  /////-----------behaviors---------------
  void flock(vector<Boid*> bl)
  {
    ali = alignment(bl);
    coh = cohesion(bl);
    sep = seperation(bl);
    acc.add(ali.mult(1));
    acc.add(coh.mult(3));
    acc.add(sep.mult(1));
  }
   
  void scatter()
  {
     
  }
  ////------------------------------------
     
  void move()
  {
    vel.add(acc); //add acceleration to velocity
	vel.limit(maxSpeed); //make sure the velocity vector magnitude does not exceed maxSpeed
	pos.add(vel); //add velocity to position
    acc.mult(0); //reset acceleration
  }
   
  void checkBounds()
  {
    if(pos.x>width) pos.x=-width;
    if(pos.x<-width) pos.x=width;

    if(pos.y>height) pos.y=-height;
    if(pos.y<-height) pos.y=height;

    if(pos.z>600) pos.z=-600;
    if(pos.z<-600) pos.z=600;
  }
   
  void render()
  {
    glPushMatrix();

    glTranslated(pos.x,pos.y,pos.z);
    glRotated(atan2(-vel.z,vel.x)  ,0,1,0);
	glRotated(asin(vel.y/vel.length())  ,0,0,1);

	glColor3f(1,1,1);
	glutSolidCone(10,20,4,1);

    glPopMatrix();
  }
   
  /*
  //steering. If arrival==true, the boid slows to meet the target. Credit to Craig Reynolds
  Vector3 steer(Vector3 target,bool arrival)
  {
    Vector3 steer = Vector3(); //creates vector for steering
    if(!arrival)
    {
      steer = target - pos; //steering vector points towards target (switch target and pos for avoiding)
      steer.limit(maxSteerForce); //limits the steering force to maxSteerForce
    }
    else
    {
      Vector3 targetOffset = target - pos;
	  double distance=targetOffset.length();
      double rampedSpeed = maxSpeed*(distance/100);
      double clippedSpeed = min(rampedSpeed,maxSpeed);

	  Vector3 desiredVelocity = targetOffset.mult(clippedSpeed/distance);
      steer = desiredVelocity  - vel;
    }
    return steer;
  }
   
   
   
   */
  //avoid. If weight == true avoidance vector is larger the closer the boid is to the target
  Vector3 avoid(Vector3 target,bool weight)
  {
    Vector3 steer = Vector3(); //creates vector for steering
    steer = pos - target; //steering vector points away from target
    if(weight){
		steer.mult(1/ sqrt( (pos-target).length() ) );
	}
    //steer.limit(maxSteerForce); //limits the steering force to maxSteerForce
    return steer;
  }

  Vector3 seperation(vector<Boid*> boids)
  {
    Vector3 posSum = Vector3(0,0,0);
    Vector3 repulse;
    for(int i=0;i<(int)boids.size();i++)
    {
      Boid* b = boids.at(i);
      
	  double d = (pos - b->pos).length();

      if(d>0&&d<=neighborhoodRadius)
      {
        repulse = pos - b->pos;

        repulse.normalize();
        repulse.div(d);
        posSum.add(repulse);
      }
    }
    return posSum;
  }

  Vector3 alignment(vector<Boid*> boids)
  {
    Vector3 velSum = Vector3(0,0,0);
    int count = 0;
    for(int i=0;i<(int)boids.size();i++)
    {
      Boid* b = boids.at(i);
      double d = (pos - b->pos).length();

      if(d>0&&d<=neighborhoodRadius)
      {
        velSum.add(b->vel);
        count++;
      }
    }

    if(count>0)
    {
      velSum.div((double)count);
      velSum.limit(maxSteerForce);
    }
    return velSum;
  }
   
  Vector3 cohesion(vector<Boid*> boids)
  {
    Vector3 posSum = Vector3(0,0,0);
    Vector3 steer = Vector3(0,0,0);
    int count = 0;

    for(int i=0;i<(int)boids.size();i++)
    {
      Boid* b = boids.at(i);
      double d = (pos - b->pos).length(); //// WAS A 2D DISTANCE

      if(d>0&&d<=neighborhoodRadius)
      {
        posSum.add(b->pos);
        count++;
      }
    }
    if(count>0)
    {
      posSum.div((double)count);
    }
    steer = posSum - pos;
    steer.limit(maxSteerForce);
    return steer;
  }

};

}