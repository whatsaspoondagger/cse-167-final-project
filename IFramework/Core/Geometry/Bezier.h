#pragma once

#include <vector>

#include <Core\Math\Matrix4.h>
#include <Core\Math\Vector4.h>
#include <Core\Math\Vector3.h>

using namespace std;

namespace IFramework{

class Bezier
{
private:
	int nDivSeg;
	int nSegs;
	vector<Vector3> controlPoints;

	vector<Vector3*> vertices;
	vector<Vector3*> tangents;

	void genCurve(void);

public:

	static bool DRAWING_CURVE;

	Bezier(vector<Vector3> ctrlPts, int nDiv);
	~Bezier(void);

	void render(void);
	vector<Vector3*> * getVertices(void);
	vector<Vector3*> * getTangents(void);
};

}