#include "Bezier.h"
#include <iostream>
#include <GL/glut.h>

namespace IFramework{

bool Bezier::DRAWING_CURVE = false;

Bezier::Bezier(vector<Vector3> ctrlPts, int nDiv)
{
	nDivSeg = nDiv;
	nSegs = (ctrlPts.size() - 1) / 3;
	controlPoints = ctrlPts;

	if (nSegs*3 + 1 != ctrlPts.size()) {
		cerr << "\nERROR CREATING BEZIER CURVE - Invalid number of control points" << endl;
		return;
	}

	genCurve();
}

Bezier::~Bezier(void)
{
	vertices.clear();
}

void Bezier::genCurve(void) {
	double t;

	Vector4 tVec;
	Vector4 dtVec;
	Vector4 xt;
	Vector4 dxt;

	Matrix4 cMat;
	cMat.identity();

	// Initialize Berstein matrix
	Matrix4 bMat = Matrix4();
	bMat.set(0, 0, 0);
	bMat.set(1, 1, 0);
	bMat.set(2, 2, 0);
	bMat.set(3, 3,  0);

	bMat.set(0, 0, -1);
	bMat.set(0, 1,  3);
	bMat.set(0, 2, -3);
	bMat.set(0, 3,  1);
	bMat.set(1, 0,  3);
	bMat.set(1, 1, -6);
	bMat.set(1, 2,  3);
	bMat.set(2, 0, -3);
	bMat.set(2, 1,  3);
	bMat.set(3, 0,  1);

	// w value of each point is 1
	Matrix4 gMat = Matrix4();
	gMat.set(0, 3, 1);
	gMat.set(1, 3, 1);
	gMat.set(2, 3, 1);
	gMat.set(3, 3, 1);

	for (int seg = 0; seg < nSegs; ++seg) {
		// Load control points into G matrix
		gMat.set(0, 0, controlPoints.at(seg*3 + 0).x);
		gMat.set(0, 1, controlPoints.at(seg*3 + 0).y);
		gMat.set(0, 2, controlPoints.at(seg*3 + 0).z);
		gMat.set(1, 0, controlPoints.at(seg*3 + 1).x);
		gMat.set(1, 1, controlPoints.at(seg*3 + 1).y);
		gMat.set(1, 2, controlPoints.at(seg*3 + 1).z);
		gMat.set(2, 0, controlPoints.at(seg*3 + 2).x);
		gMat.set(2, 1, controlPoints.at(seg*3 + 2).y);
		gMat.set(2, 2, controlPoints.at(seg*3 + 2).z);
		gMat.set(3, 0, controlPoints.at(seg*3 + 3).x);
		gMat.set(3, 1, controlPoints.at(seg*3 + 3).y);
		gMat.set(3, 2, controlPoints.at(seg*3 + 3).z);
		
		//gMat.print();
		//bMat.print();

		cMat =  gMat * bMat;
		//cMat.print();

		for (int n = 0; n < nDivSeg; ++n) {
			t = n * (1.f/nDivSeg);
			tVec = Vector4(t*t*t, t*t, t, 1);
			dtVec = Vector4(3*t*t, 2*t, 1, 0);
			
			xt = cMat *  tVec;
			dxt = cMat * dtVec;

			vertices.push_back(new Vector3( xt.x, xt.y, xt.z ) );
			tangents.push_back(new Vector3( dxt.x, dxt.y, dxt.z ) );						
		}
	}

	t = 1;
	tVec = Vector4(t*t*t, t*t, t, 1);
	dtVec = Vector4(3*t*t, 2*t, 1, 0);
			
	xt = cMat *  tVec;
	dxt = cMat * dtVec;

	vertices.push_back(new Vector3( xt.x, xt.y, xt.z ) );
	tangents.push_back(new Vector3( dxt.x, dxt.y, dxt.z ) );

}

void Bezier::render(void) {

	if(DRAWING_CURVE){

	vector<Vector3*>::iterator i;

	glColor3f(1, 1, 1);

	glBegin(GL_LINE_STRIP);
	for (i = vertices.begin(); i < vertices.end(); ++i) {
		Vector3 *v = *i;
		glVertex3f(v->x, v->y, v->z);
	}
	glEnd();

	glColor3f(0, 1, 0);

	vector<Vector3>::iterator j;

	glBegin(GL_LINE_STRIP);
	for (j = controlPoints.begin(); j < controlPoints.end(); ++j) {
		Vector3 v = *j;
		glVertex3f(v.x, v.y, v.z);
	}
	glEnd();

	}
}

vector<Vector3*> * Bezier::getVertices(void) {
	return &vertices;
}

vector<Vector3*> * Bezier::getTangents(void) {
	return &tangents;
}

}