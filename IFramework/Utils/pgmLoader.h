#ifndef PGM_LOADER
#define PGM_LOADER

#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

namespace IFramework{

	unsigned char* loadPGM(string filename, int& width, int& height);

}

#endif;