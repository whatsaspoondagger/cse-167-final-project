#include "Utils.h"

#define _USE_MATH_DEFINES
#include <math.h>

namespace IFramework {

double Utils::degreeToRad(double degree)
{
	return (degree*M_PI)/180;
}

double Utils::radToDegree(double rad)
{
	return rad*180/(M_PI);
}

void Utils::drawGird(int x, int y, double space){

	/*
		for (int n = 0; n < size; ++n) {
			for (int m = 0; m < size; ++m) {

				//glTexCoord2f((double)m/(double)(size-1),(double)n/(double)(size-1));
				glTexCoord2f(0,0);
				glVertex3f( m*grid_size, 0,n*grid_size);

				//glTexCoord2f((double)m/(double)(size-1),(double)(n+1)/(double)(size-1));
				glTexCoord2f(1,0);
				glVertex3f(	m*grid_size, 0,(n+1)*grid_size);

				//glTexCoord2f((double)(m+1)/(double)(size-1),(double)(n+1)/(double)(size-1));
				glTexCoord2f(1,1);
				glVertex3f( (m+1)*grid_size, 0,(n+1)*grid_size);

				//glTexCoord2f((double)(m+1)/(double)(size-1),(double)n/(double)(size-1));
				glTexCoord2f(0,1);
				glVertex3f( (m+1)*grid_size, 0,(n)*grid_size);
			}
		}
	*/
}

void Utils::limit(double& value, double min, double max){

	if(value < min){
		value = min;
	}

	if(value > max){
		value = max;
	}
}

//// There is problem here : Reference http://www.songho.ca/opengl/gl_anglestoaxes.html
void Utils::anglesToAxes(Vector3 angles, Vector3& left, Vector3& up, Vector3& forward)
{
    const double DEG2RAD = 3.141593f / 180;
    double sx, sy, sz, cx, cy, cz, theta;

    // rotation angle about X-axis (pitch)
    theta = (angles.x * DEG2RAD);
    sx = sin(theta);
    cx = cos(theta);

    // rotation angle about Y-axis (yaw)
    theta = -(3.141593f + angles.y * DEG2RAD);
    sy = sin(theta);
    cy = cos(theta);

    // rotation angle about Z-axis (roll)
    theta = angles.z * DEG2RAD;
    sz = sin(theta);
    cz = cos(theta);

    // camera transforms are in reverse order: RzRyRx
    // determine left axis
    left.x = cz*cy;
    left.y = sz*cy;
    left.z = -sy;

    // determine up axis
    up.x = -sz*cx + cz*sy*sx;
    up.y = cz*cx + sz*sy*sx;
    up.z = cy*sx;

    // determine forward axis
    forward.x = sz*sx + cz*sy*cx;
    forward.y = -cz*sx + sz*sy*cx;
    forward.z = cy*cx;
}


double Utils::random(double a, double b)
{
	return ((b-a)*((double)rand()/RAND_MAX))+a;
}

/// http://stackoverflow.com/questions/2306172/malloc-a-3-dimensional-array-in-c
double *** Utils::alloc_data(size_t xlen, size_t ylen, size_t zlen)
{
    double ***p;
    size_t i, j;

    if ((p = (double***)malloc(xlen * sizeof *p)) == NULL) {
        perror("malloc 1");
        return NULL;
    }

    for (i=0; i < xlen; ++i)
        p[i] = NULL;

    for (i=0; i < xlen; ++i)
        if ((p[i] = (double**)malloc(ylen * sizeof *p[i])) == NULL) {
            perror("malloc 2");
            free_data(p, xlen, ylen);
            return NULL;
        }

    for (i=0; i < xlen; ++i)
        for (j=0; j < ylen; ++j)
            p[i][j] = NULL;

    for (i=0; i < xlen; ++i)
        for (j=0; j < ylen; ++j)
            if ((p[i][j] = (double*)malloc(zlen * sizeof *p[i][j])) == NULL) {
                perror("malloc 3");
                free_data(p, xlen, ylen);
                return NULL;
            }

    return p;
}

void Utils::free_data(double ***data, size_t xlen, size_t ylen)
{
    size_t i, j;

    for (i=0; i < xlen; ++i) {
        if (data[i] != NULL) {
            for (j=0; j < ylen; ++j)
                free(data[i][j]);
            free(data[i]);
        }
    }
    free(data);
}

char* Utils::stringToCharPointer(string a)
{
      char* x = new char[a.size()+1];

      for (int i=0; i<(int)a.size(); i++)
      {
            x[i]=a[i];
      }
      
	  x[a.size()]='\0';
      
	  return x;
}

void Utils::drawNormalLines(double (*vertices)[3],double (*normals)[3], int size)
{
	glBegin (GL_LINES);
	glColor3f(0, 0, 1);

	for (unsigned int i = 0; i <= size; ++i) {

		glVertex3f(vertices[i][0],vertices[i][1],vertices[i][2]);
		glVertex3f(vertices[i][0] + normals[i][0],vertices[i][1] + normals[i][1],vertices[i][2] + normals[i][2]);
	}

	glEnd();
}

void Utils::spherePoint( double* v, double radius, double theta, double gamma ) {

	double x = radius * sin( theta ) * cos( gamma );
	double y = radius * cos( theta );
	double z = radius * sin( theta) * sin( gamma );
	v[0] = x ;
	v[1] = y ;
	v[2] = z ;
}

}