#include "Iprogram.h"

namespace IFramework {

Group Iprogram::world;
Matrix4 Iprogram::worldMatrix;
SkyBox Iprogram::sky;
bool Iprogram::useSkyBox = false;
bool Iprogram::useWriteFrame = false;
int Iprogram::frame = 0;
int Iprogram::timebase = 0;
char Iprogram::title[80] = "IFramework program";
int Iprogram::width = 512;
int Iprogram::height = 512;
int Iprogram::TIME = 0;

#define TICKS_PER_SECOND 30
const int TIMER_MILLISECONDS = 1000 / TICKS_PER_SECOND;

/*
	Functions pointer
*/
void (* Iprogram::loopFunc)(void) = NULL;
void (* Iprogram::keyFunc)(unsigned char) = NULL;

void processAnimationTimer(int value) {

    glutTimerFunc(TIMER_MILLISECONDS, processAnimationTimer, 0);
    glutPostRedisplay();
}

void Iprogram::init(int argc, char *argv[],char* title,int width,int height){

  /*
	OpenGL initialization
  */
  Iprogram::width = width;
  Iprogram::height = height;

  glutInit(&argc, argv);      	      	      // initialize GLUT
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);   // open an OpenGL context with double buffering, RGB colors, and depth buffering
  glutInitWindowPosition(100,100);  
  glutInitWindowSize(width,height);				// set initial window size
  glutCreateWindow(title);						// open window and set window title

  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
	  /* Problem: glewInit failed, something is seriously wrong. */
	  fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
  }
  fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

  /*
	Basic settings
  */
  glEnable(GL_DEPTH_TEST);            	      // enable depth buffering
  glClear(GL_DEPTH_BUFFER_BIT);       	      // clear depth buffer
  glClearColor(0.8, 0.9, 1.0, 1.0);   	      // set clear color to black
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);  // set polygon drawing mode to fill front and back of each polygon
  glShadeModel(GL_SMOOTH);             	      // set shading to smooth

  /*
		Init default material & light settings
  */
  GLfloat global_ambient[]   = {0.1, 0.1, 0.1, 1.0};
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);  // set the default ambient color
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);  // allow glColor to set ambient and diffuse colors of geometry
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_LIGHTING);
  glEnable(GL_NORMALIZE);
  glEnable(GL_CULL_FACE);
  glEnable(GL_TEXTURE_2D);
  glDepthFunc(GL_LEQUAL);    // configure depth testing
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);          // really nice perspective calculations

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  /* Iprogram  initalization */
  TextureManager::init();
  glutSetCursor(GLUT_CURSOR_NONE);
  sky.init();
  worldMatrix.identity();

}

void Iprogram::start(){
  /// 3 callbacks
  glutDisplayFunc(displayCallback);
  glutReshapeFunc(reshapeCallback);
  glutIdleFunc(idleCallback);
  glutTimerFunc(TIMER_MILLISECONDS, processAnimationTimer, 0);

  /// Interaction
  glutKeyboardFunc(keyDown);
  glutKeyboardUpFunc(keyUp);
  glutPassiveMotionFunc(mouse);
  glutMotionFunc(mouse);

  glutMainLoop();
}

/*
	Three callback functions
*/
void Iprogram::reshapeCallback(int w, int h){
 
  if(h == 0){
	  h = 1;
  }

  float ratio = w * 1.0/ h;

  glViewport(0, 0, w, h);  // set new viewport size
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(Frustum::fov,ratio,Frustum::nearD, 160000*100);//Frustum::farD);

  Frustum::setSettings(Frustum::fov,ratio,Frustum::nearD,Frustum::farD);
}

void Iprogram::displayCallback(void){
  
  if(useWriteFrame){
	  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  }else{
	  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  // clear color and depth buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();


  Frustum::drawn=0;
  Camera::update();

  //Frustum::drawLines();

  world.draw(worldMatrix,Vector3(0,0,0));

  if(loopFunc){
	loopFunc();
  }
  
  if(useSkyBox){
	  sky.render();
  }

  calcualteFPS();

  glutSwapBuffers();
}

void Iprogram::idleCallback(void)
{
  Iprogram::displayCallback();
}

/// Calculate FPS
void Iprogram::calcualteFPS(){

  float fps,time;
  frame++;
  time=glutGet(GLUT_ELAPSED_TIME);

  if (time - timebase > 1000) {
	fps = frame*1000.0/(time-timebase);
	timebase = time;
	frame = 0;
	
	Iprogram::TIME+=1;

	sprintf(title, "FPS: %8.2f  Drawn: %8d    Total: %8d",fps, Frustum::drawn, world.numChildren() );
	glutSetWindowTitle(title);
  }
}

/*
	Interaction
*/
void Iprogram::mouse(int x, int y){
	Camera::mouseInput(x,y);
}

void Iprogram::keyDown (unsigned char key, int x, int y){

	switch(key){
	case '1':
		useWriteFrame = !useWriteFrame;
		break;
	case 27:
		exit(0);
		break;
	}

	if(keyFunc){
		keyFunc(key);
	}

	Camera::keyDown(key,x,y);
}

void Iprogram::keyUp (unsigned char key, int x, int y){
	Camera::keyUp(key,x,y);
}


}